/*
    hpcsched
    Copyright (C) 2017 German Tischler

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <config.h>

#include <runProgram.hpp>
#include <which.hpp>

#include <libmaus2/util/ArgParser.hpp>
#include <libmaus2/util/ArgInfo.hpp>
#include <libmaus2/util/TempFileNameGenerator.hpp>
#include <libmaus2/aio/OutputStreamInstance.hpp>
#include <libmaus2/aio/OutputStreamFactoryContainer.hpp>
#include <libmaus2/util/WriteableString.hpp>
#include <libmaus2/network/Socket.hpp>
#include <libmaus2/util/GetFileSize.hpp>
#include <libmaus2/util/ContainerDescriptionList.hpp>
#include <libmaus2/util/CommandContainer.hpp>
#include <libmaus2/util/stringFunctions.hpp>
#include <libmaus2/aio/InputOutputStreamInstance.hpp>
#include <libmaus2/parallel/StdTerminatableSynchronousQueue.hpp>
#include <libmaus2/digest/md5.hpp>
#include <libmaus2/parallel/LockedBool.hpp>
#include <libmaus2/parallel/StdThread.hpp>
#include <RunInfo.hpp>
#include <sys/wait.h>

#if defined(HAVE_EPOLL_CREATE) || defined(HAVE_EPOLL_CREATE1)
#include <sys/epoll.h>
#endif

#include <sys/types.h>
#include <pwd.h>

static std::string getTime()
{
	::time_t const t = ::time(0);
	struct tm * ltm = ::localtime(&t);

	std::ostringstream ostr;

	ostr
		<< std::setw(4) << std::setfill('0') << (ltm->tm_year+1900) << std::setw(0)
		<< ":"
		<< std::setw(2) << std::setfill('0') << (ltm->tm_mon+1) << std::setw(0)
		<< ":"
		<< std::setw(2) << std::setfill('0') << (ltm->tm_mday) << std::setw(0)
		<< ":"
		<< std::setw(2) << std::setfill('0') << (ltm->tm_hour) << std::setw(0)
		<< ":"
		<< std::setw(2) << std::setfill('0') << (ltm->tm_min) << std::setw(0)
		<< ":"
		<< std::setw(2) << std::setfill('0') << (ltm->tm_sec) << std::setw(0)
		;

	return ostr.str();
}

#if 0
#include <slurm/slurm.h>


struct SlurmControlConfig
{
	slurm_ctl_conf_t * conf;

	SlurmControlConfig()
	: conf(0)
	{
		int const r = slurm_load_ctl_conf(static_cast<time_t>(0),&conf);

		if ( r != 0 )
		{
			int const error = slurm_get_errno();
			libmaus2::exception::LibMausException lme;
			lme.getStream() << "[E_" << getTime() << "] slurm_load_ctl_conf failed: " << slurm_strerror(error) << std::endl;
			lme.finish();
			throw lme;
		}
	}

	~SlurmControlConfig()
	{
		slurm_free_ctl_conf(conf);
	}

	uint64_t getMaxArraySize() const
	{
		return conf->max_array_sz;
	}
};

struct SlurmPartitions
{
	partition_info_msg_t * partitions;

	SlurmPartitions()
	{
		int const r = slurm_load_partitions(static_cast<time_t>(0),&partitions,0/*flags*/);

		if ( r != 0 )
		{
			int const error = slurm_get_errno();
			libmaus2::exception::LibMausException lme;
			lme.getStream() << "[E_" << getTime() << "] slurm_load_partitions failed: " << slurm_strerror(error) << std::endl;
			lme.finish();
			throw lme;
		}
	}

	~SlurmPartitions()
	{
		slurm_free_partition_info_msg(partitions);
	}

	uint64_t size() const
	{
		return partitions->record_count;
	}

	std::string getName(uint64_t const i) const
	{
		assert ( i < size() );
		return partitions->partition_array[i].name;
	}

	uint64_t getIdForName(std::string const & name) const
	{
		for ( uint64_t i = 0; i < size(); ++i )
			if ( getName(i) == name )
				return i;

		libmaus2::exception::LibMausException lme;
		lme.getStream() << "[E_" << getTime() << "] partition named " << name << " not found" << std::endl;
		lme.finish();
		throw lme;
	}

	std::string getNodes(uint64_t const i) const
	{
		assert ( i < size() );
		return partitions->partition_array[i].nodes;
	}

	uint64_t getTotalCpus(uint64_t const i) const
	{
		assert ( i < size() );
		return partitions->partition_array[i].total_cpus;
	}

	uint64_t getTotalNodes(uint64_t const i) const
	{
		assert ( i < size() );
		return partitions->partition_array[i].total_nodes;
	}

	uint64_t getMaxTime(uint64_t const i) const
	{
		assert ( i < size() );
		return partitions->partition_array[i].max_time;
	}

	uint64_t getMaxMemPerCpu(uint64_t const i) const
	{
		assert ( i < size() );
		return partitions->partition_array[i].max_mem_per_cpu;
	}

	uint64_t getMaxCpusPerNode(uint64_t const i) const
	{
		assert ( i < size() );
		return partitions->partition_array[i].max_cpus_per_node;
	}
};



struct SlurmJobs
{
	job_info_msg_t * jobs;

	SlurmJobs()
	: jobs(0)
	{
		int const r = slurm_load_jobs(0,&jobs,0 /* showflags */);

		if ( r != 0 )
		{
			int const error = slurm_get_errno();
			libmaus2::exception::LibMausException lme;
			lme.getStream() << "[E_" << getTime() << "] slurm_load_jobs failed: " << slurm_strerror(error) << std::endl;
			lme.finish();
			throw lme;
		}
	}

	~SlurmJobs()
	{
		slurm_free_job_info_msg(jobs);
	}

	uint64_t size() const
	{
		return jobs->record_count;
	}

	uint64_t getUserId(uint64_t const i) const
	{
		return jobs->job_array[i].user_id;
	}

	std::string getUserName(uint64_t const i) const
	{
		uint64_t const uid = getUserId(i);
		struct passwd * pw = getpwuid(uid);

		if ( pw )
		{
			return std::string(pw->pw_name);
		}
		else
		{
			libmaus2::exception::LibMausException lme;
			lme.getStream() << "[E_" << getTime() << "] no user name found for uid " << uid << std::endl;
			lme.finish();
			throw lme;
		}
	}

	char const * getName(uint64_t const i) const
	{
		return jobs->job_array[i].name;
	}

	uint64_t getJobId(uint64_t const i) const
	{
		return jobs->job_array[i].job_id;
	}
};
#endif

#include <FDIO.hpp>

std::string getUsage(libmaus2::util::ArgParser const & arg)
{
	std::ostringstream ostr;

	ostr << "usage: " << arg.progname << " [<parameters>] <container.cdl>" << std::endl;
	ostr << "\n";
	ostr << "parameters:\n";
	ostr << " -t          : number of threads (defaults to number of cores on machine)\n";
	ostr << " -T          : prefix for temporary files (default: create files in current working directory)\n";
	ostr << " --workertime: time for workers in (default: 1440)\n";
	ostr << " --workermem : memory for workers (default: 40000)\n";
	ostr << " -p          : cluster partition (default: haswell)\n";
	ostr << " --workers   : number of workers (default: 16)\n";

	return ostr.str();
}

struct EPoll
{
	int fd;
	std::set<int> activeset;

	EPoll(int const
	#if defined(HAVE_EPOLL_CREATE) && !defined(HAVE_EPOLL_CREATE1)
		size
	#endif
	) : fd(-1), activeset()
	{
		#if defined(HAVE_EPOLL_CREATE1)
		fd = epoll_create1(0);

		{
			std::ostringstream ostr;
			ostr << "[V_" << getTime() << "] epoll_create1 returned " << fd << std::endl;
			std::cerr << ostr.str();
		}

		if ( fd < 0 )
		{
			int const error = errno;

			libmaus2::exception::LibMausException lme;
			lme.getStream() << "[E_" << getTime() << "] epoll_create1() failed: " << strerror(error) << std::endl;
			lme.finish();
			throw lme;
		}
		#elif defined(HAVE_EPOLL_CREATE)
		fd = epoll_create(size);

		{
			std::ostringstream ostr;
			ostr << "[V_" << getTime() << "] epoll_create(" << size << ") returned " << fd << std::endl;
			std::cerr << ostr.str();
		}

		if ( fd < 0 )
		{
			int const error = errno;

			libmaus2::exception::LibMausException lme;
			lme.getStream() << "[E_" << getTime() << "] epoll_create1() failed: " << strerror(error) << std::endl;
			lme.finish();
			throw lme;
		}

		#else
		libmaus2::exception::LibMausException lme;
		lme.getStream() << "[E_" << getTime() << "] EPoll: epoll interface not supported " << std::endl;
		lme.finish();
		throw lme;
		#endif
	}

	~EPoll()
	{
		if ( fd >= 0 )
		{
			::close(fd);
			fd = -1;
		}
	}

	#if defined(HAVE_EPOLL_CREATE) || defined(HAVE_EPOLL_CREATE1)
	void add(int const addfd)
	{
		struct epoll_event ev;
		ev.events = EPOLLIN
			#if defined(EPOLLRDHUP)
			| EPOLLRDHUP
			#endif
			;
		ev.data.fd = addfd;

		while ( true )
		{
			int const r = epoll_ctl(
				fd,
				EPOLL_CTL_ADD,
				addfd,
				&ev
			);

			if ( r == 0 )
			{
				std::ostringstream ostr;
				ostr << "[I_" << getTime() << "] adding file descriptor " << addfd << " to epoll set" << std::endl;

				libmaus2::parallel::ScopeStdSpinLock slock(libmaus2::aio::StreamLock::cerrlock);
				std::cerr << ostr.str();

				activeset.insert(addfd);
				break;
			}

			int const error = errno;

			switch ( error )
			{
				case EINTR:
				case EAGAIN:
					break;
				default:
				{

					libmaus2::exception::LibMausException lme;
					lme.getStream() << "[E_" << getTime() << "] EPoll:add: epoll_ctl() failed: " << strerror(error) << std::endl;
					lme.finish();
					throw lme;
				}
			}
		}
	}

	void remove(int const remfd)
	{
		while ( true )
		{
			int const r = epoll_ctl(
				fd,
				EPOLL_CTL_DEL,
				remfd,
				NULL
			);

			if ( r == 0 )
			{
				std::ostringstream ostr;
				ostr << "[I_" << getTime() << "] removing file descriptor " << remfd << " from epoll set" << std::endl;

				libmaus2::parallel::ScopeStdSpinLock slock(libmaus2::aio::StreamLock::cerrlock);
				std::cerr << ostr.str();

				activeset.erase(remfd);
				break;
			}
			else
			{
				int const error = errno;

				switch ( error )
				{
					case EINTR:
					case EAGAIN:
						break;
					default:
					{
						libmaus2::exception::LibMausException lme;
						lme.getStream() << "[E_" << getTime() << "] EPoll:remove: epoll_ctl() failed: " << strerror(error) << std::endl;
						lme.finish();
						throw lme;
					}
				}
			}
		}
	}

	bool wait(int & rfd, int const timeout = 1000 /* milli seconds */)
	{
		rfd = -1;

		struct epoll_event events[1];

		while ( true )
		{
			int const nfds = epoll_wait(fd, &events[0], sizeof(events)/sizeof(events[0]), timeout);

			if ( nfds < 0 )
			{
				int const error = errno;

				switch ( error )
				{
					case EINTR:
					case EAGAIN:
						break;
					default:
					{
						libmaus2::exception::LibMausException lme;
						lme.getStream() << "[E_" << getTime() << "] EPoll:wait: epoll_wait() failed: " << strerror(error) << std::endl;
						lme.finish();
						throw lme;
					}
				}
			}

			if ( nfds == 0 )
			{
				return false;
			}
			else
			{
				rfd = events[0].data.fd;

				if ( activeset.find(rfd) != activeset.end() )
				{
					return true;
				}
				else
				{
					std::ostringstream ostr;
					std::cerr << "[W_" << getTime() << "] warning: epoll returned inactive file descriptor " << rfd << std::endl;

					libmaus2::parallel::ScopeStdSpinLock slock(libmaus2::aio::StreamLock::cerrlock);
					std::cerr << ostr.str();

					return false;
				}
			}
		}
	}
	#else
	void add(int const)
	{
	}
	void remove(int const)
	{
	}
	bool wait(int &, int const = 1000 /* milli seconds */)
	{
		return false;
	}
	#endif
};


struct SlurmControl
{
	struct JobDescription
	{
		int64_t containerid;
		int64_t subid;

		JobDescription() : containerid(-1), subid(-1)
		{
		}
		JobDescription(uint64_t const rcontainerid, uint64_t const rsubid)
		: containerid(rcontainerid), subid(rsubid)
		{

		}

		bool operator<(JobDescription const & O) const
		{
			if ( containerid != O.containerid )
				return containerid < O.containerid;
			else if ( subid != O.subid )
				return subid < O.subid;
			else
				return false;
		}

		void reset()
		{
			containerid = -1;
			subid = -1;
		}
	};

	struct WorkerInfo
	{
		int64_t id;
		libmaus2::network::SocketBase::unique_ptr_type Asocket;
		bool active;
		JobDescription packageid;
		uint64_t workerid;
		std::string wtmpbase;
		uint64_t workerthreads;

		std::string outdatafn;
		std::string errdatafn;
		std::string metafn;
		std::string nodename;

		WorkerInfo() { reset(0); }

		void reset(uint64_t * activeslots)
		{
			id = -1;
			Asocket.reset();
			active = false;
			workerid = std::numeric_limits<uint64_t>::max();
			workerthreads = 0;
			outdatafn = std::string();
			errdatafn = std::string();
			metafn = std::string();
			nodename = std::string();
			resetPackageId();

			if ( activeslots )
				*activeslots -= 1;
		}

		void resetPackageId()
		{
			packageid.reset();
		}
	};

	struct StartWorkerRequest
	{
		uint64_t * nextworkerid;
		std::string tmpfilebase;
		std::string hostname;
		uint64_t serverport;
		uint64_t workertime;
		uint64_t workermem;
		uint64_t workerthreads;
		std::string partition;
		libmaus2::util::ArgParser const * arg;
		WorkerInfo * AW;
		uint64_t i;
		std::map<uint64_t,uint64_t> * idToSlot;
		uint64_t workers;
		libmaus2::util::TempFileNameGenerator * tmpgen;

		StartWorkerRequest() {}
		StartWorkerRequest(
			uint64_t & rnextworkerid,
			std::string rtmpfilebase,
			std::string rhostname,
			uint64_t rserverport,
			uint64_t rworkertime,
			uint64_t rworkermem,
			uint64_t rworkerthreads,
			std::string rpartition,
			libmaus2::util::ArgParser const & rarg,
			WorkerInfo * rAW,
			uint64_t ri,
			std::map<uint64_t,uint64_t> & ridToSlot,
			uint64_t rworkers,
			libmaus2::util::TempFileNameGenerator * rtmpgen
		) :
			nextworkerid(&rnextworkerid),
			tmpfilebase(rtmpfilebase),
			hostname(rhostname),
			serverport(rserverport),
			workertime(rworkertime),
			workermem(rworkermem),
			workerthreads(rworkerthreads),
			partition(rpartition),
			arg(&rarg),
			AW(rAW),
			i(ri),
			idToSlot(&ridToSlot),
			workers(rworkers),
			tmpgen(rtmpgen)
		{

		}

		void dispatch()
		{
			uint64_t const workerid = (*nextworkerid)++;
			std::ostringstream workernamestr;
			workernamestr << "worker_" << workerid;
			std::string const workername = workernamestr.str();

			std::ostringstream wtmpbasestr;
			wtmpbasestr << tmpgen->getFileName() << "_" << workerid;
			std::string const wtmpbase = wtmpbasestr.str();

			std::ostringstream outfnstr;
			outfnstr << wtmpbase << ".out";
			std::string const outfn = outfnstr.str();

			std::string const descname = wtmpbase + "_worker.sbatch";

			std::ostringstream commandstr;
			commandstr << "hpcschedworker " << hostname << " " << serverport;
			std::string command = commandstr.str();

			writeJobDescription(
				descname,
				workername,
				outfn,
				workertime,
				workermem,
				workerthreads,
				partition,
				command
			);

			std::vector<std::string> Varg;
			Varg.push_back("sbatch");
			Varg.push_back(descname);

			std::string const jobid_s = runProgram(Varg,*arg);

			std::deque<std::string> Vtoken = libmaus2::util::stringFunctions::tokenize(jobid_s,std::string(" "));

			if ( Vtoken.size() >= 4 )
			{
				std::istringstream istr(Vtoken[3]);
				uint64_t id;
				istr >> id;

				if ( istr && istr.peek() == '\n' )
				{
					// std::cerr << "got job id " << id << std::endl;

					AW [ i ].id = id;
					AW [ i ].workerid = workerid;
					AW [ i ].wtmpbase = wtmpbase;
					AW [ i ].workerthreads = workerthreads;
					(*idToSlot)[id] = i;
				}
				else
				{
					libmaus2::exception::LibMausException lme;
					lme.getStream() << "[E_" << getTime() << "] unable to find job id in " << jobid_s << std::endl;
					lme.finish();
					throw lme;
				}
			}
			else
			{
				libmaus2::exception::LibMausException lme;
				lme.getStream() << "[E_" << getTime() << "] unable to find job id in " << jobid_s << std::endl;
				lme.finish();
				throw lme;
			}

			libmaus2::aio::FileRemoval::removeFile(descname);

			{
				std::ostringstream ostr;
				ostr << "[V_" << getTime() << "] started job " << (i+1) << " out of " << workers << " with id " << AW[i].id << std::endl;
				std::cerr << ostr.str();
			}
		}
	};



	struct ProgState
	{
		uint64_t numunfinished;
		uint64_t numpending;

		ProgState() : numunfinished(0), numpending(0) {}
		ProgState(
			uint64_t const rnumunfinished,
			uint64_t const rnumpending
		) : numunfinished(rnumunfinished), numpending(rnumpending)
		{

		}

		bool operator==(ProgState const & o) const
		{
			return numunfinished == o.numunfinished && numpending == o.numpending;
		}

		bool operator!=(ProgState const & o) const
		{
			return !operator==(o);
		}
	};


	struct WriteContainerRequest
	{
		libmaus2::util::ContainerDescription object;
		uint64_t offset;

		WriteContainerRequest() {}
		WriteContainerRequest(
			libmaus2::util::ContainerDescription const & robject,
			uint64_t const roffset
		) : object(robject), offset(roffset) {}
		WriteContainerRequest(std::istream & in)
		{
			deserialise(in);
		}

		void dispatch(std::iostream & cdlstream) const
		{
			cdlstream.clear();
			cdlstream.seekp(offset);
			object.serialise(cdlstream);
			cdlstream.flush();
		}

		std::ostream & serialise(std::ostream & out) const
		{
			object.serialise(out);
			libmaus2::util::NumberSerialisation::serialiseNumber(out,offset);
			return out;
		}

		std::string serialise() const
		{
			std::ostringstream ostr;
			serialise(ostr);
			return ostr.str();
		}

		std::ostream & serialiseWithChecksum(std::ostream & ostr) const
		{
			std::string const s = serialise();
			ostr.write(s.c_str(),s.size());

			std::string c;
			libmaus2::util::MD5::md5(s,c);
			libmaus2::util::StringSerialisation::serialiseString(ostr,c);

			ostr.flush();

			return ostr;
		}

		std::ostream & serialiseWithChecksumNoFlush(std::ostream & ostr) const
		{
			std::string const s = serialise();
			ostr.write(s.c_str(),s.size());

			std::string c;
			libmaus2::util::MD5::md5(s,c);
			libmaus2::util::StringSerialisation::serialiseString(ostr,c);

			return ostr;
		}

		void serialiseWithChecksum(std::string const & fn) const
		{
			libmaus2::aio::OutputStreamInstance OSI(fn);
			serialiseWithChecksum(OSI);
		}

		std::istream & deserialise(std::istream & in)
		{
			object.deserialise(in);
			offset = libmaus2::util::NumberSerialisation::deserialiseNumber(in);
			return in;
		}

		bool deserialiseWithChecksum(std::istream & in)
		{
			try
			{
				deserialise(in);
				std::string const c_in = libmaus2::util::StringSerialisation::deserialiseString(in);
				std::string c;

				libmaus2::util::MD5::md5(serialise(),c);

				return c == c_in;
			}
			catch(std::exception const & ex)
			{
				std::ostringstream ostr;
				ostr << "[E_" << getTime() << "] " << ex.what() << std::endl;
				std::cerr << ostr.str();
				return false;
			}
		}

		static bool deserialiseWithChecksumStatic(std::istream & in)
		{
			WriteContainerRequest WCR;
			return WCR.deserialiseWithChecksum(in);
		}

		static bool deserialiseWithChecksum(std::string const & in)
		{
			try
			{
				libmaus2::aio::InputStreamInstance ISI(in);
				return deserialiseWithChecksumStatic(ISI);
			}
			catch(std::exception const & ex)
			{
				std::ostringstream ostr;
				ostr << "[E_" << getTime() << "] " << ex.what() << std::endl;
				std::cerr << ostr.str();
				return false;
			}
		}
	};

	struct WriteContainerRequestList
	{
		std::vector < WriteContainerRequest > V;

		WriteContainerRequestList() : V() {}

		void dispatch(std::iostream & cdlstream) const
		{
			for ( uint64_t i = 0; i < V.size(); ++i )
				V[i].dispatch(cdlstream);
		}

		std::ostream & serialiseWithChecksum(std::ostream & out) const
		{
			libmaus2::util::NumberSerialisation::serialiseNumber(out,V.size());
			for ( uint64_t i = 0; i < V.size(); ++i )
				V[i].serialiseWithChecksumNoFlush(out);
			out.flush();

			return out;
		}

		void serialiseWithChecksum(std::string const & out) const
		{
			libmaus2::aio::OutputStreamInstance OSI(out);
			serialiseWithChecksum(OSI);
		}

		bool deserialiseWithChecksum(std::istream & in)
		{
			V.resize(libmaus2::util::NumberSerialisation::deserialiseNumber(in));

			bool ok = true;
			for ( uint64_t i = 0; i < V.size(); ++i )
				ok = ok && V[i].deserialiseWithChecksum(in);

			return ok;
		}

		bool deserialiseWithChecksum(std::string const & fn)
		{
			libmaus2::aio::InputStreamInstance ISI(fn);
			return deserialiseWithChecksum(ISI);
		}

		static bool deserialiseWithChecksumStatic(std::string const & fn)
		{
			WriteContainerRequestList O;
			return O.deserialiseWithChecksum(fn);
		}
	};

	std::string const curdir;
	unsigned short serverport;
	uint64_t const backlog;
	uint64_t const tries;
	uint64_t nextworkerid;
	std::string const hostname;
	std::string const tmpfilebase;
	libmaus2::util::TempFileNameGenerator tmpgen;
	uint64_t const workertime;
	uint64_t const workermem;
	std::string const partition;
	uint64_t const workers;
	std::string const cdl;
	libmaus2::aio::InputOutputStreamInstance::shared_ptr_type cdlstream;
	libmaus2::aio::InputStreamInstance::shared_ptr_type cdloffsetstream;

	libmaus2::autoarray::AutoArray<WorkerInfo> AW;
	uint64_t activeslots;
	std::map<uint64_t,uint64_t> idToSlot;
	std::map<int,uint64_t> fdToSlot;
	std::map < JobDescription, uint64_t > Mfail;

	uint64_t const CDLn;
	// libmaus2::util::ContainerDescriptionList CDL;

	std::set < JobDescription > Sunfinished;
	std::set < JobDescription > Srunning;
	std::set<uint64_t> Sresubmit;
	uint64_t ndeepsleep;
	std::map < uint64_t, uint64_t > Munfinished;

	uint64_t const maxthreads;
	uint64_t const workerthreads;

	EPoll EP;

	libmaus2::network::ServerSocket::unique_ptr_type Pservsock;

	std::set<uint64_t> restartSet;
	std::set<uint64_t> wakeupSet;
	// uint64_t pending;

	ProgState pstate;
	bool failed;

	std::vector < StartWorkerRequest > Vreq;

	libmaus2::aio::InputOutputStreamInstance metastream;

	static void writeJobDescription(
		std::string const & fn,
		std::string const & jobname,
		std::string const & outfn,
		uint64_t const utime,
		uint64_t const umem,
		uint64_t const threads,
		std::string const partition,
		std::string const command
	)
	{
		libmaus2::aio::OutputStreamInstance OSI(fn);

		OSI << "#!/bin/bash\n";
		OSI << "#SBATCH --job-name=" << jobname << "\n";
		OSI << "#SBATCH --output=" << outfn << "\n";
		OSI << "#SBATCH --ntasks=1" << "\n";
		OSI << "#SBATCH --time=" << utime << "\n";
		OSI << "#SBATCH --mem=" << umem << "\n";
		OSI << "#SBATCH --cpus-per-task=" << threads << "\n";
		OSI << "#SBATCH --cpus-per-task=" << threads << "\n";
		OSI << "#SBATCH --partition=" << partition << "\n";
		OSI << "srun bash -c \"" << command << "\"\n";
	}

	void processWakeupSet()
	{
		for ( std::set < uint64_t >::const_iterator it = wakeupSet.begin();
			it != wakeupSet.end(); ++it )
		{
			uint64_t const i = *it;
			{
				std::ostringstream ostr;
				ostr << "[V_" << getTime() << "] sending wakeup to slot " << i << std::endl;
				std::cerr << ostr.str();
			}

			FDIO fdio(AW[i].Asocket->getFD());
			fdio.writeNumber(1);
		}
		wakeupSet.clear();
	}

	void processResubmitSet()
	{
		for ( uint64_t c = 0; c < 100 && Sresubmit.size(); ++c )
		{
			std::vector<uint64_t> Vresubmit;

			for ( std::set < uint64_t >::const_iterator it = Sresubmit.begin();
				it != Sresubmit.end(); ++it )
			{
				uint64_t const i = *it;
				{
					std::ostringstream ostr;
					ostr << "[V_" << getTime() << "] resubmitting slot " << i << " after deep sleep" << std::endl;
					std::cerr << ostr.str();
				}

				try
				{
					Vreq[i].dispatch();
					Vresubmit.push_back(i);
				}
				catch(std::exception const & ex)
				{
					std::ostringstream ostr;
					ostr << "[E_" << getTime() << "] job start failed:\n" << ex.what() << std::endl;
					AW[i].reset(0);
				}

			}

			for ( uint64_t i = 0; i < Vresubmit.size(); ++i )
				Sresubmit.erase(Vresubmit[i]);

			if ( Sresubmit.size() )
				::sleep(10);
		}
	}

	static libmaus2::util::ContainerDescriptionList loadCDL(std::string const & cdl)
	{
		libmaus2::util::ContainerDescriptionList CDL;
		libmaus2::aio::InputStreamInstance ISI(cdl);
		CDL.deserialise(ISI);
		return CDL;
	}

	static uint64_t loadCDLn(std::istream & istr)
	{
		istr.clear();
		istr.seekg(0);
		return libmaus2::util::NumberSerialisation::deserialiseNumber(istr);
	}

	void seekCDLOffset(uint64_t const i) const
	{
		cdloffsetstream->clear();
		cdloffsetstream->seekg(-static_cast<int64_t>((CDLn+1) * sizeof(uint64_t)) + static_cast<int64_t>(i * sizeof(uint64_t)),std::ios::end);
	}

	uint64_t getCDLPointer(uint64_t const i) const
	{
		// std::cerr << "[V_" << getTime() << "] getting pointer for entry " << i << std::endl;
		seekCDLOffset(i);
		return libmaus2::util::NumberSerialisation::deserialiseNumber(*cdloffsetstream);
	}

	std::vector<uint64_t> getCDLPointers(std::vector<uint64_t> V) const
	{
		std::vector<uint64_t> O;

		uint64_t low = 0;
		while ( low < V.size() )
		{
			uint64_t high = low+1;

			while ( high < V.size() && V[high] == V[high-1]+1 )
				++high;

			seekCDLOffset(V[low]);

			for ( uint64_t j = low; j < high; ++j )
				O.push_back(libmaus2::util::NumberSerialisation::deserialiseNumber(*cdloffsetstream));

			low = high;
		}

		return O;
	}

	std::vector< std::pair<uint64_t,uint64_t> > getCDLEntryLengths(std::vector<uint64_t> V) const
	{
		// output vector
		std::vector< std::pair<uint64_t,uint64_t> > O;

		// while we have entries to process
		uint64_t low = 0;
		while ( low < V.size() )
		{
			// get consecutive interval
			uint64_t high = low+1;

			while ( high < V.size() && V[high] == V[high-1]+1 )
				++high;

			seekCDLOffset(V[low]);

			std::pair<uint64_t,uint64_t> P;
			P.first = libmaus2::util::NumberSerialisation::deserialiseNumber(*cdloffsetstream);
			P.second = libmaus2::util::NumberSerialisation::deserialiseNumber(*cdloffsetstream);
			O.push_back(P);

			for ( uint64_t j = low+1; j < high; ++j )
			{
				P.first = P.second;
				P.second = libmaus2::util::NumberSerialisation::deserialiseNumber(*cdloffsetstream);
				O.push_back(P);
			}

			low = high;
		}

		return O;
	}

	uint64_t getCDLEntryLength(uint64_t const i) const
	{
		return getCDLPointer(i+1) - getCDLPointer(i);
	}

	libmaus2::util::ContainerDescription loadContainerDescription(uint64_t const i) const
	{
		// std::cerr << "[V_" << getTime() << "] loading description for " << i << std::endl;
		uint64_t const ptr = getCDLPointer(i);
		cdlstream->clear();
		cdlstream->seekg(ptr);
		libmaus2::util::ContainerDescription CD(*cdlstream);
		return CD;
	}

	void checkDependencies()
	{
		for ( uint64_t i = 0; i < CDLn; ++i )
		{
			libmaus2::util::ContainerDescription CD = loadContainerDescription(i);
			libmaus2::util::CommandContainer const CC = decodeContainer(CD);

			uint64_t completedep = 0;

			for ( uint64_t i = 0; i < CC.depid.size(); ++i )
			{
				uint64_t const d = CC.depid[i];
				libmaus2::util::ContainerDescription const DCD = loadContainerDescription(d);
				libmaus2::util::CommandContainer const DCC = decodeContainer(DCD);

				if ( DCC.isComplete() )
					completedep += 1;
			}

			uint64_t const expmissingdep = CC.depid.size() - completedep;

			if ( expmissingdep != CD.missingdep )
			{
				std::cerr << "container " << i << " completedep=" << completedep << " missingdep=" << CD.missingdep << " depid.size()=" << CC.depid.size() << std::endl;

				uint64_t const cp = getCDLPointer(i);
				CD.missingdep = expmissingdep;

				WriteContainerRequest const WCR(CD,cp);
				WCR.dispatch(*cdlstream);
			}

			if ( i % 1024 == 0 )
				std::cerr << "[V] checkDependencies " << i / static_cast<double>(CDLn) << std::endl;
		}
	}

	void countUnfinished()
	{
		std::cerr << "[V_" << getTime() << "] counting unfinished\n";

		// count number of unfinished jobs per command container
		cdlstream->clear();
		cdlstream->seekg(0);
		uint64_t const n = libmaus2::util::NumberSerialisation::deserialiseNumber(*cdlstream);

		uint64_t numincomplete = 0;

		for ( uint64_t i = 0; i < n; ++i )
		{
			uint64_t const p = cdlstream->tellg();
			uint64_t const cp = getCDLPointer(i);
			bool const pointerok = (p == cp);

			if ( ! pointerok )
			{
				std::cerr << "[V_" << getTime() << "] pointer " << p << " :: " << cp << std::endl;
				assert ( pointerok );
			}

			libmaus2::util::ContainerDescription CD(*cdlstream);
			std::istringstream istr(CD.fn);

			libmaus2::util::CommandContainer const CC(istr);
			uint64_t numunfinished = 0;

			for ( uint64_t j = 0; j < CC.V.size(); ++j )
				if ( CC.V[j].completed )
				{

				}
				else
				{
					Munfinished[i]++;
					numunfinished += 1;
				}

			if ( numunfinished )
				numincomplete += 1;

			#if 0
			// if ( numunfinished )
			{
				std::ostringstream ostr;
				ostr << "[V_" << getTime() << "] container " << i << " has " << numunfinished << " unfinished jobs" << std::endl;
				std::cerr << ostr.str();
			}
			#endif

			#if 0
			if ( numunfinished )
			{
				// check reverse dependencies
				for ( uint64_t j = 0; j < CC.rdepid.size(); ++j )
				{
					uint64_t const k = CC.rdepid[j];

					std::cerr << "[V_" << getTime() << "] container " << k << " has missing dependency " << i << std::endl;

					updateMissingDep(k,1);
				}
			}
			#endif
		}

		std::cerr << "[V_" << getTime() << "] counted unfinished, found " << numincomplete << "\n";
	}

	uint64_t computeMaxThreads() const
	{
		uint64_t maxthreads = 1;

		std::cerr << "[V_" << getTime() << "] computeMaxThreads\n";

		cdlstream->clear();
		cdlstream->seekg(0);
		uint64_t const n = libmaus2::util::NumberSerialisation::deserialiseNumber(*cdlstream);

		for ( uint64_t i = 0; i < n; ++i )
		{
			libmaus2::util::ContainerDescription CD(*cdlstream);
			std::istringstream istr(CD.fn);

			libmaus2::util::CommandContainer const CC(istr);

			maxthreads = std::max(maxthreads,CC.threads);
		}

		return maxthreads;
	}


	void addUnfinished(JobDescription const J)
	{
		Sunfinished.insert(J);
	}

	JobDescription getUnfinished()
	{
		std::set< JobDescription >::const_iterator const it = Sunfinished.begin();
		JobDescription const P = *it;
		Sunfinished.erase(it);
		return P;
	}

	void enqueUnfinished()
	{
		std::cerr << "[V_" << getTime() << "] enqueUnfinished\n";

		cdlstream->clear();
		cdlstream->seekg(0);
		uint64_t const n = libmaus2::util::NumberSerialisation::deserialiseNumber(*cdlstream);

		for ( uint64_t i = 0; i < n; ++i )
		{
			uint64_t const p = cdlstream->tellg();
			uint64_t const cp = getCDLPointer(i);
			bool const pointerok = (p == cp);

			if ( ! pointerok )
			{
				std::cerr << "[V_" << getTime() << "] pointer " << p << " :: " << cp << std::endl;
				assert ( pointerok );
			}

			libmaus2::util::ContainerDescription const CD(*cdlstream);

			if ( CD.missingdep == 0 )
			{
				std::istringstream istr(CD.fn);
				libmaus2::util::CommandContainer const CC(istr);
				uint64_t q = 0;

				std::ostringstream ostr;

				{
					ostr << "[V_" << getTime() << "] container " << i << " has no missing dependencies, enqueuing jobs" << std::endl;
				}

				for ( uint64_t j = 0; j < CC.V.size(); ++j )
				{
					if ( !CC.V[j].completed )
					{
						addUnfinished(JobDescription(i,j));
						ostr << "\tenqueing (" << i << "," << j << ")" << std::endl;
						ostr << "\t\t" << CC.V[j] << std::endl;
						++q;

						// get command
						libmaus2::util::Command const com = getCommand(i,j);
						ostr << "\t\t\t" << com << std::endl;
					}
				}

				if ( q )
				{
					std::cerr << ostr.str();
				}
			}
		}
	}

	std::vector < StartWorkerRequest > computeStartRequests(libmaus2::util::ArgParser const & arg)
	{
		std::vector < StartWorkerRequest > Vreq(workers);
		for ( uint64_t i = 0; i < workers; ++i )
			Vreq[i] = StartWorkerRequest(
				nextworkerid,tmpfilebase,hostname,serverport,
				workertime,workermem,workerthreads,partition,arg,AW.begin(),i,
				idToSlot,workers,&tmpgen
			);
		return Vreq;
	}

	void checkRequeue(uint64_t const slotid)
	{
		Mfail [ AW[slotid].packageid ] += 1;

		libmaus2::util::CommandContainer const CC = getCommandContainer(AW[slotid].packageid.containerid);
		libmaus2::util::Command const CO = getCommand(AW[slotid].packageid.containerid,AW[slotid].packageid.subid);

		// mark pipeline as failed
		if ( Mfail [ AW[slotid].packageid ] >= CC.maxattempt )
		{
			{
				std::ostringstream ostr;
				ostr << "[V_" << getTime() << "] too many failures on " << AW[slotid].packageid.containerid << "," << AW[slotid].packageid.subid << ", marking pipeline as failed" << std::endl;
				std::cerr << ostr.str();
			}

			if ( !CO.ignorefail )
				failed = true;
		}
		// requeue
		else
		{
			{
				std::ostringstream ostr;
				ostr << "[V_" << getTime() << "] requeuing " << AW[slotid].packageid.containerid << "," << AW[slotid].packageid.subid << std::endl;
				std::cerr << ostr.str();
			}

			addUnfinished(AW[slotid].packageid);
			processWakeupSet();
			processResubmitSet();
		}
	}


	void resetSlot(uint64_t const slotid)
	{
		uint64_t const id = AW[slotid].id;
		EP.remove(AW[slotid].Asocket->getFD());
		fdToSlot.erase(AW[slotid].Asocket->getFD());
		AW[slotid].reset(&activeslots);
		idToSlot.erase(id);
		wakeupSet.erase(id);
		restartSet.insert(slotid);
	}

	void writeDescription(libmaus2::util::ContainerDescription const & CD, uint64_t const i)
	{
		// read offset for description
		uint64_t const offset = getCDLPointer(i);
		uint64_t const length = getCDLEntryLength(i);

		assert ( CD.serialisedSize() == length );

		// instantiate request
		WriteContainerRequestList WL;
		WL.V = std::vector < WriteContainerRequest >(1,WriteContainerRequest(CD,offset));

		// get name of journal on disk
		std::string const journalname = getJournalName();

		// serialise request with checksum
		WL.serialiseWithChecksum(journalname);

		// see whether we can read it back with checksum correct
		bool const ok = WriteContainerRequestList::deserialiseWithChecksumStatic(journalname);

		// if not then fail
		if ( ! ok )
		{
			libmaus2::exception::LibMausException lme;
			lme.getStream() << "[E_" << getTime() << "] failed to read back journal entry" << std::endl;
			lme.finish();
			throw lme;
		}

		// update on disk information
		WL.dispatch(*cdlstream);
		cdlstream->flush();

		libmaus2::aio::FileRemoval::removeFile(journalname);
	}

	libmaus2::util::CommandContainer getCommandContainer(uint64_t const containerid) const
	{
		libmaus2::util::ContainerDescription CD = loadContainerDescription(containerid);
		std::istringstream istr(CD.fn);
		libmaus2::util::CommandContainer CC(istr);
		return CC;
	}

	libmaus2::util::Command getCommand(uint64_t const containerid, uint64_t const subid) const
	{
		libmaus2::util::CommandContainer const CC = getCommandContainer(containerid);
		libmaus2::util::Command const & CO = CC.V[subid];
		return CO;
	}

	libmaus2::util::ContainerDescription updateCommandSuccessful(uint64_t const containerid, uint64_t const subid)
	{
		libmaus2::util::CommandContainer CC = getCommandContainer(containerid);
		libmaus2::util::Command & CO = CC.V[subid];
		CO.numattempts += 1;
		CO.completed = true;

		std::ostringstream ostr;
		CC.serialise(ostr);

		libmaus2::util::ContainerDescription CD = loadContainerDescription(containerid);
		CD.fn = ostr.str();

		writeDescription(CD,containerid);

		return CD;
	}

	void updateCommandFailed(uint64_t const containerid, uint64_t const subid)
	{
		libmaus2::util::CommandContainer CC = getCommandContainer(containerid);
		libmaus2::util::Command & CO = CC.V[subid];
		CO.numattempts += 1;

		std::ostringstream ostr;
		CC.serialise(ostr);

		libmaus2::util::ContainerDescription CD = loadContainerDescription(containerid);
		CD.fn = ostr.str();

		writeDescription(CD,containerid);
	}

	void updateCommandDecrementAttempts(uint64_t const containerid, uint64_t const subid)
	{
		libmaus2::util::CommandContainer CC = getCommandContainer(containerid);
		libmaus2::util::Command & CO = CC.V[subid];
		CO.numattempts -= 1;

		std::ostringstream ostr;
		CC.serialise(ostr);

		libmaus2::util::ContainerDescription CD = loadContainerDescription(containerid);
		CD.fn = ostr.str();

		writeDescription(CD,containerid);
	}

	libmaus2::util::CommandContainer decodeContainer(libmaus2::util::ContainerDescription const & CD)
	{
		std::istringstream istr(CD.fn);
		return libmaus2::util::CommandContainer(istr);
	}

	void handleSuccessfulCommand(
		uint64_t const slotid,
		bool const = false /* verbose = false */
	)
	{
		JobDescription const packageid = AW[slotid].packageid;

		libmaus2::util::ContainerDescription const CDU = updateCommandSuccessful(packageid.containerid,packageid.subid);

		if ( decodeContainer(CDU).V[packageid.subid].deepsleep )
		{
			assert ( ndeepsleep > 0 );
			ndeepsleep -= 1;
		}
		Srunning.erase(packageid);

		uint64_t const numunfin = --Munfinished [ packageid.containerid ];

		if ( !numunfin )
		{
			{
				std::ostringstream ostr;
				ostr << "[V_" << getTime() << "] finished command container " << packageid.containerid << std::endl;
				std::cerr << ostr.str();
			}

			// get command container
			libmaus2::util::CommandContainer CC = decodeContainer(CDU);
			// sort rdepid
			std::sort(CC.rdepid.begin(),CC.rdepid.end());
			// vector (rdepid,count)
			std::vector< std::pair<uint64_t,uint64_t> > U;

			// collect intervals of equal rdepid
			{
				uint64_t ilow = 0;
				while ( ilow < CC.rdepid.size() )
				{
					uint64_t ihigh = ilow+1;
					while ( ihigh < CC.rdepid.size() && CC.rdepid[ihigh] == CC.rdepid[ilow] )
						++ihigh;

					U.push_back(std::pair<uint64_t,uint64_t>(CC.rdepid[ilow],ihigh-ilow));

					ilow = ihigh;
				}
			}

			bool checkwake = false;

			uint64_t const blocksize = 1024;
			uint64_t const nent = U.size();
			uint64_t const numblocks = (nent + blocksize - 1)/blocksize;

			// iterate over blocks
			for ( uint64_t b = 0; b < numblocks; ++b )
			{
				// block bounds on rdepid
				uint64_t const blow = b * blocksize;
				uint64_t const bhigh = std::min(blow+blocksize,nent);

				// get sub vector
				std::vector < std::pair<uint64_t,uint64_t> > USUB(U.begin() + blow, U.begin() + bhigh);
				// extract id vector
				std::vector < uint64_t > UI(USUB.size());
				for ( uint64_t i = 0; i < USUB.size(); ++i )
					UI[i] = USUB[i].first;
				// get entry lengths vector
				std::vector< std::pair<uint64_t,uint64_t> > Vent = getCDLEntryLengths(UI);

				std::vector<libmaus2::util::ContainerDescription> VCD(bhigh-blow);
				std::vector<WriteContainerRequest> VWCR(bhigh-blow);

				for ( uint64_t i = 0; i < Vent.size(); ++i )
				{
					if ( (i==0) || (Vent[i-1].second != Vent[i].first) )
					{
						cdlstream->clear();
						cdlstream->seekg(Vent[i].first);
					}

					VCD[i] = libmaus2::util::ContainerDescription(*cdlstream);
					assert ( VCD[i].missingdep >= USUB[i].second );
					assert ( VCD[i].missingdep > 0 );
					VCD[i].missingdep -= USUB[i].second;
					VWCR[i] = WriteContainerRequest(VCD[i],Vent[i].first);
				}

				WriteContainerRequestList WCRL;
				WCRL.V = VWCR;

				// get name of journal on disk
				std::string const journalname = getJournalName();

				// serialise request with checksum
				WCRL.serialiseWithChecksum(journalname);

				if ( WriteContainerRequestList::deserialiseWithChecksumStatic(journalname) )
				{

				}
				else
				{
					libmaus2::exception::LibMausException lme;
					lme.getStream() << "[E_" << getTime() << "] failed to read back journal data" << std::endl;
					lme.finish();
					throw lme;
				}

				// now serialise objects
				for ( uint64_t i = 0; i < Vent.size(); ++i )
				{
					if ( (i==0) || (Vent[i-1].second != Vent[i].first) )
					{
						cdlstream->clear();
						cdlstream->seekp(Vent[i].first);
					}

					assert ( cdlstream->tellp() == static_cast<int64_t>(Vent[i].first) );

					assert ( VCD[i].serialisedSize() == Vent[i].second - Vent[i].first );

					VCD[i].serialise(*cdlstream);
				}

				cdlstream->flush();

				// delete journal file
				libmaus2::aio::FileRemoval::removeFile(journalname);

				for ( uint64_t i = 0; i < USUB.size(); ++i )
				{
					if ( VCD[i].missingdep == 0 )
					{
						uint64_t const k = USUB[i].first;

						libmaus2::util::CommandContainer const NCCV = decodeContainer(VCD[i]);
						for ( uint64_t j = 0; j < NCCV.V.size(); ++j )
						{
							addUnfinished(JobDescription(k,j));
							checkwake = true;
						}
					}
				}


				// ZZZ
			}

			if ( checkwake )
			{
				processWakeupSet();
				processResubmitSet();
			}
		}

		AW[slotid].resetPackageId();
	}

	void handleFailedCommand(uint64_t const slotid)
	{
		JobDescription const packageid = AW[slotid].packageid;

		updateCommandFailed(packageid.containerid,packageid.subid);

		libmaus2::util::CommandContainer const CC = getCommandContainer(packageid.containerid);
		libmaus2::util::Command const CO = getCommand(packageid.containerid,packageid.subid);
		if ( CO.deepsleep )
		{
			assert ( ndeepsleep > 0 );
			ndeepsleep -= 1;
		}
		Srunning.erase(packageid);

		if ( CO.numattempts >= CC.maxattempt && CO.ignorefail )
		{
			std::ostringstream ostr;

			ostr << "[V_" << getTime() << "] number of attempts reached max " << CC.maxattempt << " but container has ignorefail flag set" << std::endl;

			ostr << "[V_" << getTime() << "] decreasing numattempts" << std::endl;
			updateCommandDecrementAttempts(packageid.containerid,packageid.subid);
			if ( CO.deepsleep )
				ndeepsleep += 1;
			Srunning.insert(packageid);
			ostr << "[V_" << getTime() << "] decreased numattempts to " << CO.numattempts << std::endl;

			ostr << "[V_" << getTime() << "] calling handleSuccesfulCommand" << std::endl;
			handleSuccessfulCommand(slotid,true);
			ostr << "[V_" << getTime() << "] returned from handleSuccesfulCommand" << std::endl;

			std::cerr << ostr.str();
		}
		else
		{
			// writeContainer(packageid.containerid);
			checkRequeue(slotid);
			AW[slotid].resetPackageId();
		}
	}

	static std::string getJournalName(std::string const & cdl)
	{
		return cdl + ".journal";
	}

	std::string getJournalName()
	{
		return getJournalName(cdl);
	}

	SlurmControl(
		std::string const & rtmpfilebase,
		uint64_t const rworkertime,
		uint64_t const rworkermem,
		std::string const rpartition,
		uint64_t const rworkers,
		std::string const & rcdl,
		int64_t const rworkerthreads,
		libmaus2::util::ArgParser const & rarg
	)
	: curdir(libmaus2::util::ArgInfo::getCurDir()),
	  serverport(50000), backlog(1024), tries(1000), nextworkerid(0), hostname(libmaus2::network::GetHostName::getHostName()),
	  tmpfilebase(rtmpfilebase),
	  tmpgen(tmpfilebase+"_tmpgen",3),
	  workertime(rworkertime),
	  workermem(rworkermem),
	  partition(rpartition),
	  workers(rworkers),
	  cdl(rcdl),
	  cdlstream(new libmaus2::aio::InputOutputStreamInstance(std::string("cpp:") + cdl,std::ios::in | std::ios::out | std::ios::binary)),
	  cdloffsetstream(new libmaus2::aio::InputStreamInstance(cdl)),
	  AW(workers),
	  activeslots(0),
	  idToSlot(),
	  fdToSlot(),
	  Mfail(),
	  CDLn(loadCDLn(*cdlstream)),
	  // CDL(loadCDL(cdl)),
	  Sunfinished(),
	  Srunning(),
	  ndeepsleep(0),
	  Munfinished(),
	  maxthreads(computeMaxThreads()),
	  workerthreads(rworkerthreads > 0 ? rworkerthreads : maxthreads),
	  EP(workers+1),
	  Pservsock(
		libmaus2::network::ServerSocket::allocateServerSocket(
			serverport,
			backlog,
			hostname,
			tries
		)
	  ),
	  restartSet(),
	  wakeupSet(),
	  // pending(0),
	  pstate(),
	  failed(false),
	  Vreq(computeStartRequests(rarg)),
	  metastream(cdl + ".meta",std::ios::in | std::ios::out | std::ios::binary)
	{

		metastream.seekp(0,std::ios::end);

		std::cerr << "[V_" << getTime() << "] hostname=" << hostname << " serverport=" << serverport << " number of containers " << CDLn << std::endl;

		std::cerr << "[V_" << getTime() << "] got server fd " << Pservsock->getFD() << std::endl;
		EP.add(Pservsock->getFD());
		fdToSlot[Pservsock->getFD()] = std::numeric_limits<uint64_t>::max();

		if ( rarg.uniqueArgPresent("checkDependencies") )
			checkDependencies();
		countUnfinished();
		enqueUnfinished();

		#if 0
		WCRQT.start();
		#endif
	}

	~SlurmControl()
	{
		#if 0
		WCRQ.terminate();
		WCRQT.join();
		#endif
	}

	int process()
	{
		for ( uint64_t i = 0; i < workers; ++i )
			Vreq[i].dispatch();

		libmaus2::util::TempFileNameGenerator tmpgen(tmpfilebase+"_tmpgen",3);

		while ( Sunfinished.size() || Srunning.size() )
		{
			std::set<uint64_t> nrestartSet;
			for ( std::set<uint64_t>::const_iterator it = restartSet.begin(); it != restartSet.end(); ++it )
			{
				uint64_t const i = *it;

				try
				{
					Vreq[i].dispatch();
				}
				catch(std::exception const & ex)
				{
					std::cerr << "[E_" << getTime() << "] job start failed:\n" << ex.what() << std::endl;
					nrestartSet.insert(i);
					AW[i].reset(0);
				}
			}
			restartSet.clear();
			restartSet = nrestartSet;

			ProgState npstate(
				Sunfinished.size(),
				Srunning.size()
			);

			if ( npstate != pstate )
			{
				pstate = npstate;

				std::ostringstream ostr;
				ostr << "[V_" << getTime() << "] Sunfinished.size()=" << pstate.numunfinished << " pending=" << pstate.numpending << std::endl;
				std::cerr << ostr.str();
			}

			int rfd = -1;
			if ( EP.wait(rfd) )
			{
				std::map<int,uint64_t>::const_iterator itslot = fdToSlot.find(rfd);

				if ( itslot == fdToSlot.end() )
				{
					libmaus2::exception::LibMausException lme;
					lme.getStream() << "[E_" << getTime() << "] EPoll::wait returned unknown file descriptor" << std::endl;
					lme.finish();
					throw lme;
				}
				else if ( itslot->second == std::numeric_limits<uint64_t>::max() )
				{
					assert ( rfd == Pservsock->getFD() );
					int64_t slot = -1;

					try
					{
						libmaus2::network::SocketBase::unique_ptr_type nptr = Pservsock->accept();

						nptr->setKeepAlive(
							1 /* enable */,
							60 /* timeout */,
							20 /* number of checks */,
							10 /* interval between checks */
						);

						FDIO fdio(nptr->getFD());
						uint64_t const jobid = fdio.readNumber();

						{
							std::ostringstream ostr;
							ostr << "[V_" << getTime() << "] accepted connection for jobid=" << jobid << " fd " << nptr->getFD() << std::endl;
							std::cerr << ostr.str();
						}

						if ( idToSlot.find(jobid) != idToSlot.end() )
						{
							slot = idToSlot.find(jobid)->second;
							fdio.writeNumber(AW[slot].workerid);
							fdio.writeString(curdir);
							bool const curdirok = fdio.readNumber();

							if ( curdirok )
							{
								fdio.writeString(AW[slot].wtmpbase);
								std::string const outdatafn = fdio.readString();
								std::string const errdatafn = fdio.readString();
								std::string const metafn = fdio.readString();
								std::string const nodename = fdio.readString();
								fdio.writeNumber(AW[slot].workerthreads);

								AW[slot].outdatafn = outdatafn;
								AW[slot].errdatafn = errdatafn;
								AW[slot].metafn = metafn;
								AW[slot].nodename = nodename;

								if ( ! AW[slot].Asocket )
								{
									AW[slot].Asocket = UNIQUE_PTR_MOVE(nptr);
									EP.add(AW[slot].Asocket->getFD());
									fdToSlot[AW[slot].Asocket->getFD()] = slot;
									AW[slot].active = true;

									{
										std::ostringstream ostr;
										ostr << "[V_" << getTime() << "] marked slot " << slot << " active for jobid " << AW[slot].id << std::endl;
										std::cerr << ostr.str();
									}
									activeslots++;
								}
								else
								{
									libmaus2::exception::LibMausException lme;
									lme.getStream() << "[E_" << getTime() << "] erratic worker trying to open second connection" << std::endl;
									lme.finish();
									throw lme;
								}
							}
						}
						else
						{
							std::cerr << "[V_" << getTime() << "] job id unknown\n";
						}
					}
					catch(std::exception const & ex)
					{
						{
							std::ostringstream ostr;
							ostr << "[E_" << getTime() << "] error while accepting new connection:\n" << ex.what() << std::endl;
							std::cerr << ostr.str();
						}

						if ( slot >= 0 )
							AW[slot].reset(0);
					}
				}
				else
				{
					std::ostringstream estr;

					uint64_t const i = itslot->second;

					estr << "[V_" << getTime() << "] epoll returned slot " << i << " ready for reading" << std::endl;

					if ( ! AW[i].active )
					{
						libmaus2::exception::LibMausException lme;
						lme.getStream() << "[E_" << getTime() << "] epoll returned file descriptor for inactive slot" << std::endl;
						lme.finish();
						throw lme;
					}

					try
					{
						// read worker state
						FDIO fdio(AW[i].Asocket->getFD());
						uint64_t const rd = fdio.readNumber();

						// worker is idle
						if ( rd == 0 )
						{
							if ( Sunfinished.size() )
							{
								// get next package
								JobDescription const currentid = getUnfinished();
								// get command
								libmaus2::util::Command const com = getCommand(currentid.containerid,currentid.subid);
								// serialise command to string
								std::ostringstream ostr;
								com.serialise(ostr);
								// process command
								AW[i].packageid = currentid;
								fdio.writeNumber(0);
								fdio.writeString(ostr.str());
								fdio.writeNumber(currentid.containerid);
								fdio.writeNumber(currentid.subid);
								std::string const sruninfo = fdio.readString();

								Srunning.insert(currentid);
								if ( com.deepsleep )
									ndeepsleep += 1;

								estr << "[V_" << getTime() << "] started " << com << " for " << currentid.containerid << "," << currentid.subid << " on slot " << i << " node " << AW[i].nodename << " wtmpbase " << AW[i].wtmpbase << std::endl;
							}
							else
							{
								if ( ndeepsleep == Srunning.size() )
								{
									// put slot to deep sleep
									estr << "[V_" << getTime() << "] putting slot " << i << " to deep sleep" << std::endl;

									// request termination
									fdio.writeNumber(2);
									EP.remove(AW[i].Asocket->getFD());
									fdToSlot.erase(AW[i].Asocket->getFD());
									AW[i].reset(&activeslots);
									Sresubmit.insert(i);
								}
								else
								{
									estr << "[V_" << getTime() << "] putting slot " << i << " in wakeupSet" << std::endl;

									wakeupSet.insert(i);
								}
							}
						}
						// worker has finished a job (may or may not be succesful)
						else if ( rd == 1 )
						{
							uint64_t const status = fdio.readNumber();
							int const istatus = static_cast<int>(status);
							std::string const sruninfo = fdio.readString();
							RunInfo const RI(sruninfo);
							RI.serialise(metastream);
							metastream.flush();
							// acknowledge
							fdio.writeNumber(0);

							estr << "[V_" << getTime() << "] slot " << i << " reports job ended with istatus=" << istatus << std::endl;

							if ( WIFEXITED(istatus) && (WEXITSTATUS(istatus) == 0) )
							{
								handleSuccessfulCommand(i);
							}
							else
							{
								estr << "[V_" << getTime() << "] slot " << i << " failed, checking requeue " << AW[i].packageid.containerid << "," << AW[i].packageid.subid << std::endl;
								handleFailedCommand(i);
							}
						}
						// worker is still running a job
						else if ( rd == 2 )
						{
							// acknowledge
							fdio.writeNumber(0);
						}
						else
						{
							estr << "[V_" << getTime() << "] process for slot " << i << " jobid " << AW[i].id << " is erratic" << std::endl;

							if ( AW[i].packageid.containerid >= 0 )
							{
								handleFailedCommand(i);
							}

							resetSlot(i /* slotid */);
						}
					}
					catch(std::exception const & ex)
					{
						estr << "[V_" << getTime() << "] exception for slot " << i << " jobid " << AW[i].id;
						if ( AW[i].packageid.containerid >= 0 )
							estr << " package (" << AW[i].packageid.containerid << "," << AW[i].packageid.subid << ") node " << AW[i].nodename << " err " << AW[i].errdatafn;
						estr << std::endl;
						estr << ex.what() << std::endl;

						if ( AW[i].packageid.containerid >= 0 )
						{
							try
							{
								handleFailedCommand(i);
							}
							catch(std::exception const & ex)
							{
								estr << "[E_" << getTime() << "] exception in handleFailedCommand: " << std::endl;
								estr << ex.what() << std::endl;
								throw;
							}
						}

						try
						{
							resetSlot(i /* slotid */);
						}
						catch(std::exception const & ex)
						{
							estr << "[E_" << getTime() << "] exception in resetSlot: " << std::endl;
							estr << ex.what() << std::endl;
							throw;
						}
					}

					std::cerr << estr.str();
				}
			}
		}

		processWakeupSet();
		processResubmitSet();

		std::set<uint64_t> Sactive;
		for ( uint64_t i = 0; i < AW.size(); ++i )
			if ( AW[i].active )
				Sactive.insert(i);

		while ( Sactive.size() )
		{
			std::vector < uint64_t > Vterm;

			for ( std::set<uint64_t>::const_iterator it = Sactive.begin(); it != Sactive.end(); ++it )
			{
				uint64_t const i = *it;

				try
				{
					// read worker state
					FDIO fdio(AW[i].Asocket->getFD());
					uint64_t const rd = fdio.readNumber();

					// worker is idle
					if ( rd == 0 )
					{
						// request terminate
						fdio.writeNumber(2);
						EP.remove(AW[i].Asocket->getFD());
						fdToSlot.erase(AW[i].Asocket->getFD());
						AW[i].reset(&activeslots);
						Vterm.push_back(i);
					}
					else if ( rd == 1 )
					{
						std::cerr << "[V_" << getTime() << "] slot " << i << " reports finished job with no jobs active" << std::endl;
					}
					// worker is still running a job
					else if ( rd == 2 )
					{
						std::cerr << "[V_" << getTime() << "] slot " << i << " reports job running, but we know of no such job" << std::endl;
						fdio.writeNumber(0);
					}
					else
					{
						std::cerr << "[V_" << getTime() << "] process for slot " << i << " jobid " << AW[i].id << " is erratic" << std::endl;

						resetSlot(i /* slotid */);
					}
				}
				catch(...)
				{
					std::cerr << "[V_" << getTime() << "] exception for slot " << i << " jobid " << AW[i].id << std::endl;

					resetSlot(i /* slotid */);
				}
			}

			for ( uint64_t i = 0; i < Vterm.size(); ++i )
				Sactive.erase(Vterm[i]);
		}

		if ( failed )
		{
			std::cerr << "[E_" << getTime() << "] pipeline failed" << std::endl;
			return EXIT_FAILURE;
		}
		else
		{
			std::cerr << "[V_" << getTime() << "] pipeline finished ok" << std::endl;
			return EXIT_SUCCESS;
		}
	}
};

struct HPCSchedServer : libmaus2::parallel::StdThreadCallable
{
	unsigned short port;
	std::string hostname;
	libmaus2::network::ServerSocket::unique_ptr_type psock;
	libmaus2::parallel::LockedBool running;
	EPoll EP;

	std::map < uint64_t, libmaus2::network::SocketBase::shared_ptr_type > Vsock;
	std::map < int, uint64_t > fdmap;
	uint64_t nextid;

	std::map < std::string, std::set<std::string> > munfinished;
	std::map < uint64_t, std::pair<std::string,std::string> > mrunning;
	std::map < std::string, std::set<std::string> > mfinished;

	HPCSchedServer(
		unsigned short rport,
		uint64_t const backlog,
		uint64_t const tries
	) : port(rport), hostname(libmaus2::network::GetHostName::getHostName()),
	    psock(libmaus2::network::ServerSocket::allocateServerSocket(port,backlog,hostname,tries)),
	    running(true), EP(2048), nextid(0)
	{
		EP.add(psock->getFD());
	}

	virtual void run()
	{
		while ( running.get() )
		{
			try
			{
				int fd = -1;
				if ( EP.wait(fd) )
				{
					if ( fd == psock->getFD() )
					{
						try
						{
							libmaus2::network::SocketBase::shared_ptr_type sock(psock->acceptShared());
							uint64_t const id = nextid++;
							Vsock[id] = sock;
							EP.add(sock->getFD());
							fdmap[sock->getFD()] = id;
						}
						catch(std::exception const & ex)
						{
							std::cerr << "[E_" << getTime() << "] HPCSchedServer::run::acceptShared:\n" << ex.what() << std::endl;
						}
					}
					else
					{
						if ( fdmap.find(fd) != fdmap.end() )
						{
							uint64_t const id = fdmap.find(fd)->second;

							try
							{
								FDIO fdio(fd);

								std::string const bucket = fdio.readString();
								uint64_t const reqtype = fdio.readNumber();

								if ( reqtype == 0 )
								{
									if (
										munfinished.find(bucket) == munfinished.end()
										||
										munfinished.find(bucket)->second.size() == 0
									)
									{
										fdio.writeNumber(0);
									}
									else
									{
										std::string const pack = *(munfinished.find(bucket)->second.begin());

										munfinished.find(bucket)->second.erase(pack);
										mrunning[id] = std::pair<std::string,std::string>(bucket,pack);

										fdio.writeNumber(1);
										fdio.writeString(pack);
									}
								}
								// finished packet
								else if ( reqtype == 1 )
								{
									if ( mrunning.find(id) != mrunning.end() )
									{
										std::pair<std::string,std::string> const P = mrunning.find(id)->second;
										mrunning.erase(id);
										mfinished[P.first].insert(P.second);
									}
								}
							}
							catch(std::exception const & ex)
							{
								EP.remove(fd);
								fdmap.erase(fd);
								Vsock.erase(id);

								if ( mrunning.find(id) != mrunning.end() )
								{
									std::pair<std::string,std::string> const P = mrunning.find(id)->second;
									munfinished[P.first].insert(P.second);
								}
							}
						}
					}
				}
			}
			catch(std::exception const & ex)
			{
				std::cerr << "[E_" << getTime() << "] HPCSchedServer::run:\n" << ex.what() << std::endl;
			}
		}
	}
};

int hpcschedcontrol(libmaus2::util::ArgParser const & arg)
{
	std::string const hpcschedworker = which("hpcschedworker");
	std::cerr << "[V_" << getTime() << "] found hpcschedworker at " << hpcschedworker << std::endl;

	std::string const tmpfilebase = arg.uniqueArgPresent("T") ? arg["T"] : libmaus2::util::ArgInfo::getDefaultTmpFileName(arg.progname);
	uint64_t const workertime = arg.uniqueArgPresent("workertime") ? arg.getParsedArg<uint64_t>("workertime") : 1440;
	uint64_t const workermem = arg.uniqueArgPresent("workermem") ? arg.getParsedArg<uint64_t>("workermem") : 40000;
	std::string const partition = arg.uniqueArgPresent("p") ? arg["p"] : "haswell";
	uint64_t const workers = arg.uniqueArgPresent("workers") ? arg.getParsedArg<uint64_t>("workers") : 16;

	std::string const cdl = arg[0];
	std::string const cdlmeta = cdl + ".meta";
	std::string const cdljournal = SlurmControl::getJournalName(cdl);

	// check for journal
	if ( libmaus2::util::GetFileSize::fileExists(cdljournal) )
	{
		SlurmControl::WriteContainerRequestList WCR;
		bool const ok = WCR.deserialiseWithChecksum(cdljournal);

		if ( ok )
		{
			libmaus2::aio::InputOutputStreamInstance::shared_ptr_type cdlstream(
				new libmaus2::aio::InputOutputStreamInstance(cdl,std::ios::in | std::ios::out | std::ios::binary)
			);
			WCR.dispatch(*cdlstream);
		}

		libmaus2::aio::FileRemoval::removeFile(cdljournal);
	}

	#if 0
	bool const journalok = SlurmControl::WCRQWriterThread::tryApplyJournal(cdl,cdljournal);

	if ( ! journalok )
	{
		std::cerr << "[E_" << getTime() << "] failed to replay journal, please check error messages" << std::endl;
		return EXIT_FAILURE;
	}
	#endif

	if ( ! libmaus2::util::GetFileSize::fileExists(cdlmeta) )
	{
		libmaus2::aio::OutputStreamInstance OSI(cdlmeta);
	}

	SlurmControl SC(
		tmpfilebase,workertime,workermem,partition,workers,cdl,
		arg.uniqueArgPresent("workerthreads") ? arg.getParsedArg<uint64_t>("workerthreads") : -1,
		arg
	);

	int const r = SC.process();

	return r;
}

int main(int argc, char * argv[])
{
	try
	{
		libmaus2::util::ArgParser const arg(argc,argv);


		if ( arg.argPresent("h") || arg.argPresent("help") )
		{
			std::cerr << getUsage(arg);
			return EXIT_SUCCESS;
		}
		else if ( arg.argPresent("version") )
		{
			std::cerr << "This is " << PACKAGE_NAME << " version " << PACKAGE_VERSION << std::endl;
			return EXIT_SUCCESS;
		}
		else if ( arg.size() < 1 )
		{
			std::cerr << getUsage(arg);
			return EXIT_FAILURE;
		}

		std::cerr << "[V_" << getTime() << "] hpcschedcontrol started" << std::endl;

		int const r = hpcschedcontrol(arg);

		return r;
	}
	catch(std::exception const & ex)
	{
		std::cerr << "[E_" << getTime() << "] exception in main: " << ex.what() << std::endl;
		return EXIT_FAILURE;
	}
}
