/*
    libmaus2
    Copyright (C) 2018 German Tischler-Höhle

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <libmaus2/types/types.hpp>
#include <libmaus2/exception/LibMausException.hpp>
#include <libmaus2/util/Base64.hpp>
#include <libmaus2/util/StringSerialisation.hpp>
#include <ModObject.hpp>
#include <string>
#include <cstring>

#include <sys/stat.h>
#include <sys/types.h>

static void srmdir(std::string const & sdir)
{
	int r = -1;

	while ( r < 0 )
	{
		r = rmdir(sdir.c_str());

		if ( r < 0 )
		{
			int const error = errno;

			switch ( error )
			{
				// return success if directory to be removed does not exist
				case ENOENT:
					r = 0;
					break;
				case EINTR:
				case EAGAIN:
					break;
				default:
				{
					libmaus2::exception::LibMausException lme;
					lme.getStream() << "[E] rmdir(" << sdir << "): " << strerror(error) << std::endl;
					lme.finish();
					throw lme;
				}
			}
		}
	}

}

static int hpcsched_rmdirbatch_cpp(char const * c, uint64_t const n, char const * d, uint64_t const m)
{
	try
	{
		ModObject const M(std::string(d,m));
		std::string sdir(c,c+n);
		sdir = libmaus2::util::Base64::decode(sdir);
		std::istringstream istr(sdir);

		std::vector<std::string> V;
		while ( istr.peek() != std::istream::traits_type::eof() )
		{
			std::string const s = libmaus2::util::StringSerialisation::deserialiseString(istr);
			V.push_back(s);
		}

		int volatile gok = 1;
		#if defined(_OPENMP)
		#pragma omp parallel for num_threads(M.workerthreads) schedule(dynamic,1)
		#endif
		for ( uint64_t i = 0; i < V.size(); ++i )
		{
			try
			{
				srmdir(V[i]);
			}
			catch(std::exception const & ex)
			{
				libmaus2::parallel::ScopeStdSpinLock slock(libmaus2::aio::StreamLock::cerrlock);
				std::cerr << ex.what() << std::endl;
				gok = 0;
			}
		}

		if ( gok )
			return EXIT_SUCCESS;
		else
			return EXIT_FAILURE;
	}
	catch(std::exception const & ex)
	{
		char const * what = ex.what();
		fprintf(stderr,"%s",what);
		return EXIT_FAILURE;
	}
}

extern "C" {

	int hpcsched_rmdirbatch(char const * c, uint64_t const n, char const * d, uint64_t const m)
	{
		return hpcsched_rmdirbatch_cpp(c,n,d,m);
	}
}
