/*
    hpcsched
    Copyright (C) 2018 German Tischler-Höhle

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#if ! defined(MODOBJECT_HPP)
#define MODOBJECT_HPP

#include <libmaus2/util/NumberSerialisation.hpp>

struct ModObject
{
	uint64_t workerthreads;

	ModObject() {}
	ModObject(
		uint64_t const rworkerthreads
	) : workerthreads(rworkerthreads) {}

	ModObject(std::istream & istr)
	{
		deserialise(istr);
	}
	ModObject(std::string const & s)
	{
		deserialise(s);
	}

	std::ostream & serialise(std::ostream & ostr) const
	{
		libmaus2::util::NumberSerialisation::serialiseNumber(ostr,workerthreads);
		return ostr;
	}

	std::string serialise() const
	{
		std::ostringstream ostr;
		serialise(ostr);
		return ostr.str();
	}

	std::istream & deserialise(std::istream & istr)
	{
		workerthreads = libmaus2::util::NumberSerialisation::deserialiseNumber(istr);
		return istr;
	}

	void deserialise(std::string const & s)
	{
		std::istringstream istr(s);
		deserialise(istr);
	}
};
#endif
