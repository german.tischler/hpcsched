/*
    hpcsched
    Copyright (C) 2017 German Tischler

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <config.h>

#include <runProgram.hpp>
#include <which.hpp>

#include <libmaus2/util/ArgParser.hpp>
#include <libmaus2/util/ArgInfo.hpp>
#include <libmaus2/util/TempFileNameGenerator.hpp>
#include <libmaus2/aio/OutputStreamInstance.hpp>
#include <libmaus2/aio/OutputStreamFactoryContainer.hpp>
#include <libmaus2/util/WriteableString.hpp>
#include <libmaus2/network/Socket.hpp>
#include <libmaus2/util/GetFileSize.hpp>
#include <libmaus2/util/ContainerDescriptionList.hpp>
#include <libmaus2/util/CommandContainer.hpp>
#include <libmaus2/aio/InputOutputStreamInstance.hpp>
#include <libmaus2/parallel/StdTerminatableSynchronousQueue.hpp>
#include <libmaus2/digest/md5.hpp>
#include <libmaus2/parallel/LockedBool.hpp>

struct Update
{
	uint64_t p;
	std::string s;

	Update()
	{}
	Update(uint64_t const rp, std::string const & rs)
	: p(rp), s(rs) {}
};

int hpcschedresetfailed(libmaus2::util::ArgParser const & arg)
{
	std::string const fn = arg[0];
	libmaus2::aio::InputStreamInstance::unique_ptr_type pISI(new libmaus2::aio::InputStreamInstance(fn));

	// count number of unfinished jobs per command container
	pISI->clear();
	pISI->seekg(0);
	uint64_t const n = libmaus2::util::NumberSerialisation::deserialiseNumber(*pISI);

	std::vector<Update> VU;

	for ( uint64_t i = 0; i < n; ++i )
	{
		uint64_t const p = pISI->tellg();

		libmaus2::util::ContainerDescription CD(*pISI);
		std::istringstream istr(CD.fn);

		libmaus2::util::CommandContainer CC(istr);
		uint64_t att = 0;
		for ( uint64_t i = 0; i < CC.V.size(); ++i )
			att += CC.V[i].numattempts;

		bool const complete = CC.isComplete();

		if ( (! complete) && att )
		{
			CC.resetFailed();

			std::ostringstream ostr;
			CC.serialise(ostr);
			CD.fn = ostr.str();

			std::ostringstream cdostr;
			CD.serialise(cdostr);

			Update const U(p,cdostr.str());
			VU.push_back(U);
		}
	}

	pISI.reset();

	libmaus2::aio::InputOutputStreamInstance IOSI(std::string("cpp:") + fn,std::ios::in | std::ios::out | std::ios::binary);

	for ( uint64_t i = 0; i < VU.size(); ++i )
	{
		std::istringstream istr(VU[i].s);
		libmaus2::util::ContainerDescription CD(istr);

		std::istringstream contistr(CD.fn);
		libmaus2::util::CommandContainer CC(contistr);

		std::cerr << "Resetting\n" << CC << std::endl;

		IOSI.clear();
		IOSI.seekp(VU[i].p);

		CD.serialise(IOSI);
	}

	return EXIT_SUCCESS;
}


int main(int argc, char * argv[])
{
	try
	{
		libmaus2::util::ArgParser const arg(argc,argv);


		if ( arg.argPresent("h") || arg.argPresent("help") )
		{
			std::cerr << "usage: " << argv[0] << " <cdl>" << std::endl;
			return EXIT_SUCCESS;
		}
		else if ( arg.argPresent("version") )
		{
			std::cerr << "This is " << PACKAGE_NAME << " version " << PACKAGE_VERSION << std::endl;
			return EXIT_SUCCESS;
		}
		else if ( arg.size() < 1 )
		{
			std::cerr << "usage: " << argv[0] << " <cdl>" << std::endl;
			return EXIT_FAILURE;
		}

		int const r = hpcschedresetfailed(arg);

		return r;
	}
	catch(std::exception const & ex)
	{
		std::cerr << "[E] exception in main: " << ex.what() << std::endl;
		return EXIT_FAILURE;
	}
}
