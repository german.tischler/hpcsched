/*
    hpcsched
    Copyright (C) 2017 German Tischler

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <libmaus2/util/ArgParser.hpp>
#include <libmaus2/util/ArgInfo.hpp>
#include <libmaus2/util/LineBuffer.hpp>
#include <libmaus2/util/TempFileNameGenerator.hpp>
#include <libmaus2/util/Command.hpp>
#include <libmaus2/util/CommandContainer.hpp>
#include <libmaus2/util/ContainerDescriptionList.hpp>
#include <libmaus2/util/Base64.hpp>
#include <libmaus2/aio/SerialisedPeeker.hpp>
#include <libmaus2/parallel/NumCpus.hpp>
#include <libmaus2/sorting/SortingBufferedOutputFile.hpp>
#include <libmaus2/util/MemUsage.hpp>
#include <sstream>
#include <regex>

struct Token
{
	enum token_type { TOKEN_NEWLINE, TOKEN_SYMBOL, TOKEN_EOF, TOKEN_ESCAPE };
	token_type token;
	char sym;

	Token()
	{

	}

	Token(token_type const rtoken, char const c) : token(rtoken), sym(c) {}
};

Token getNextToken(std::istream & in)
{
	while ( in.peek() != std::istream::traits_type::eof() )
	{
		int const c = in.get();

		if ( c == '\\' )
		{
			if ( in.peek() != std::istream::traits_type::eof() )
			{
				int const c = in.get();

				// skip newline
				if ( c != '\n' )
					return Token(Token::TOKEN_ESCAPE,c);
			}
			else
			{
				return Token(Token::TOKEN_SYMBOL,c);
			}
		}
		else if ( c == '\n' )
		{
			return Token(Token::TOKEN_NEWLINE,'\n');
		}
		else
		{
			Token tok(Token::TOKEN_SYMBOL,c);
			return tok;
		}
	}

	return Token(Token::TOKEN_EOF,0);
}

struct Rule
{
	std::vector<std::string> produced;
	std::vector<std::string> dependencies;
	std::vector<std::string> commands;
	bool ignorefail;
	bool deepsleep;
	int64_t maxattempt;
	int64_t numthreads;
	int64_t mem;

	void clear()
	{
		produced.resize(0);
		dependencies.resize(0);
		commands.resize(0);
	}

	Rule()
	{

	}

	Rule(std::istream & in)
	{
		deserialise(in);
	}

	static void serialiseStringVector(std::ostream & out, std::vector<std::string> const & V)
	{
		libmaus2::util::NumberSerialisation::serialiseNumber(out,V.size());
		for ( uint64_t i = 0; i < V.size(); ++i )
			libmaus2::util::StringSerialisation::serialiseString(out,V[i]);
	}

	static void deserialiseStringVector(std::istream & in, std::vector<std::string> & V)
	{
		V.resize(libmaus2::util::NumberSerialisation::deserialiseNumber(in));
		for ( uint64_t i = 0; i < V.size(); ++i )
			V[i] = libmaus2::util::StringSerialisation::deserialiseString(in);
	}

	std::ostream & serialise(std::ostream & out) const
	{
		serialiseStringVector(out,produced);
		serialiseStringVector(out,dependencies);
		serialiseStringVector(out,commands);
		libmaus2::util::NumberSerialisation::serialiseNumber(out,ignorefail);
		libmaus2::util::NumberSerialisation::serialiseNumber(out,deepsleep);
		libmaus2::util::NumberSerialisation::serialiseNumber(out,maxattempt);
		libmaus2::util::NumberSerialisation::serialiseNumber(out,numthreads);
		libmaus2::util::NumberSerialisation::serialiseNumber(out,mem);
		return out;
	}

	std::istream & deserialise(std::istream & in)
	{
		deserialiseStringVector(in,produced);
		deserialiseStringVector(in,dependencies);
		deserialiseStringVector(in,commands);
		ignorefail = libmaus2::util::NumberSerialisation::deserialiseNumber(in);
		deepsleep = libmaus2::util::NumberSerialisation::deserialiseNumber(in);
		maxattempt = libmaus2::util::NumberSerialisation::deserialiseNumber(in);
		numthreads = libmaus2::util::NumberSerialisation::deserialiseNumber(in);
		mem = libmaus2::util::NumberSerialisation::deserialiseNumber(in);
		return in;
	}
};

std::ostream & operator<<(std::ostream & out, Rule const & R)
{
	out << "Rule:" << std::endl;
	for ( uint64_t i = 0; i < R.produced.size(); ++i )
		out << "\tproduced[" << i << "]=" << R.produced[i] << std::endl;
	for ( uint64_t i = 0; i < R.dependencies.size(); ++i )
		out << "\tdependencies[" << i << "]=" << R.dependencies[i] << std::endl;
	for ( uint64_t i = 0; i < R.commands.size(); ++i )
		out << "\t\tcommands[" << i << "]=" << R.commands[i] << std::endl;
	return out;
}

static std::vector<std::string> splitSpace(std::string const & s)
{
	uint64_t i = 0;

	std::vector<std::string> V;
	while ( i < s.size() )
	{
		while ( i < s.size() && isspace(s[i]) )
			++i;

		uint64_t const j = i;

		while ( i < s.size() && !isspace(s[i]) )
			++i;

		if ( i > j )
		{
			std::string const t(
				s.begin()+j,
				s.begin()+i
			);
			V.push_back(t);
		}
	}

	return V;
}

static uint64_t getDefaultMaxTry()
{
	return 2;
}

static uint64_t checkMaxTry(std::string const & s)
{
	std::regex R("\\{\\{maxtry(\\d+)\\}\\}");

	std::smatch sm;
	if ( ::std::regex_search(s, sm, R) )
	{
		std::istringstream istr(sm[1]);
		uint64_t i;
		istr >> i;

		if ( istr && istr.peek() == std::istream::traits_type::eof() )
		{
			return i;
		}
		else
		{
			libmaus2::exception::LibMausException lme;
			lme.getStream() << "[E] cannot parse maxtry parameter in " << s << std::endl;
			lme.finish();
			throw lme;
		}
	}
	else
	{
		return getDefaultMaxTry();
	}
}

static uint64_t getDefaultNumThreads()
{
	return 1;
}

static uint64_t checkNumThreads(std::string const & s)
{
	std::regex R("\\{\\{threads(\\d+)\\}\\}");

	std::smatch sm;
	if ( ::std::regex_search(s, sm, R) )
	{
		std::istringstream istr(sm[1]);
		uint64_t i;
		istr >> i;

		if ( istr && istr.peek() == std::istream::traits_type::eof() )
		{
			return i;
		}
		else
		{
			libmaus2::exception::LibMausException lme;
			lme.getStream() << "[E] cannot parse threads parameter in " << s << std::endl;
			lme.finish();
			throw lme;
		}
	}
	else
	{
		return getDefaultNumThreads();
	}
}

static uint64_t getDefaultMem()
{
	return 1*1024;
}

static uint64_t checkMem(std::string const & s)
{
	std::regex R("\\{\\{mem(\\d+)\\}\\}");

	std::smatch sm;
	if ( ::std::regex_search(s, sm, R) )
	{
		std::istringstream istr(sm[1]);
		uint64_t i;
		istr >> i;

		if ( istr && istr.peek() == std::istream::traits_type::eof() )
		{
			return i;
		}
		else
		{
			libmaus2::exception::LibMausException lme;
			lme.getStream() << "[E] cannot parse mem parameter in " << s << std::endl;
			lme.finish();
			throw lme;
		}
	}
	else
	{
		return getDefaultMem();
	}
}

static std::string getDefaultD(libmaus2::util::ArgParser const & arg)
{
	return libmaus2::util::ArgInfo::getDefaultTmpFileName(arg.progname);
}

struct RuleString
{
	libmaus2::autoarray::AutoArray<char> A;
	uint64_t Ao;

	RuleString() : Ao(0) {}

	static void serialise(std::ostream & out, char const * c, uint64_t const n)
	{
		out.write(reinterpret_cast<char const *>(&n),sizeof(n));
		out.write(c,n);
	}

	std::istream & deserialise(std::istream & in)
	{
		in.read(reinterpret_cast<char *>(&Ao),sizeof(Ao));
		A.ensureSize(Ao+1);
		in.read(A.begin(),Ao);
		A[Ao] = 0;
		return in;
	}
};

uint64_t parseFile(std::string const fn, std::string const & rulefn)
{
	std::string const ruletmp = rulefn + ".tmp";
	libmaus2::util::TempFileRemovalContainer::addTempFile(ruletmp);
	libmaus2::aio::OutputStreamInstance::unique_ptr_type Pruletmp(
		new libmaus2::aio::OutputStreamInstance(ruletmp)
	);

	uint64_t n = 0;
	libmaus2::aio::InputStreamInstance ISI(fn);
	libmaus2::aio::OutputStreamInstance OSI(rulefn);

	// std::vector<std::string> V;

	uint64_t numinputlines = 0;

	Token tok;
	libmaus2::autoarray::AutoArray<char> CA;
	uint64_t CAo = 0;
	while ( (tok = getNextToken(ISI)).token != Token::TOKEN_EOF )
	{
		if ( tok.token == Token::TOKEN_NEWLINE )
		{
			if ( CAo )
			{
				RuleString::serialise(*Pruletmp,CA.begin(),CAo);
				CAo = 0;

				numinputlines += 1;
				if ( (numinputlines % (1024*1024)) == 0 )
				{
					std::cerr << "[V] tokenised " << numinputlines << " input lines" << std::endl;
				}
			}
		}
		else if ( tok.token == Token::TOKEN_ESCAPE )
		{
			CA.push(CAo,'\\');
			CA.push(CAo,tok.sym);
		}
		else if ( tok.token == Token::TOKEN_SYMBOL )
		{
			CA.push(CAo,tok.sym);
		}
	}

	if ( CAo )
	{
		// V.push_back(ostr.str());
		RuleString::serialise(*Pruletmp,CA.begin(),CAo);
		CAo = 0;

		numinputlines += 1;
	}

	std::cerr << "[V] tokenised " << numinputlines << " input lines" << std::endl;

	Pruletmp->flush();
	Pruletmp.reset();

	Rule R;
	bool rulevalid = false;

	bool ignorefail = false;
	bool deepsleep = false;
	int64_t maxattempt = getDefaultMaxTry();
	int64_t numthreads = getDefaultNumThreads();
	int64_t mem = getDefaultMem();

	std::string const flagprefix = "#{{hpcschedflags}}";

	libmaus2::aio::InputStreamInstance ruletmpISI(ruletmp);
	// libmaus2::aio::SerialisedPeeker<RuleString> SP(ruletmp);
	RuleString S;
	uint64_t processedinputlines = 0;
	//for ( uint64_t i = 0; i < V.size(); ++i )
	for ( uint64_t i = 0; ruletmpISI.peek() != std::istream::traits_type::eof(); ++i )
	{
		S.deserialise(ruletmpISI);
		char const * s = S.A.begin();
		// std::string const s = S.s; // V[i];

		if ( S.Ao )
		{
			// rule command line
			if ( s[0] == '\t' )
			{
				if ( rulevalid )
				{
					R.commands.push_back(std::string(s+1,s+S.Ao));
				}
				else
				{
					libmaus2::exception::LibMausException lme;
					lme.getStream() << "[E] command line " << s << " outside any rule" << std::endl;
					lme.finish();
					throw lme;
				}
			}
			// comment
			else if ( s[0] == '#' )
			{
				if (
					S.Ao >= flagprefix.size()
					&&
					strncmp(
						flagprefix.c_str(),
						s,
						flagprefix.size()
					) == 0
					// s.substr(0,flagprefix.size()) == flagprefix
				)
				{
					std::string const f = s + flagprefix.size(); // s.substr(flagprefix.size());

					ignorefail = false;
					deepsleep = false;
					maxattempt = checkMaxTry(f);
					numthreads = checkNumThreads(f);
					mem = checkMem(f);

					if ( f.find("{{ignorefail}}") != std::string::npos )
					{
						ignorefail = true;
					}
					if ( f.find("{{deepsleep}}") != std::string::npos )
					{
						deepsleep = true;
					}
				}
			}
			else
			{
				if ( rulevalid )
				{
					R.serialise(OSI);
					n += 1;
					// VR.push_back(R);
					R.clear();
				}

				uint64_t offc = S.Ao;
				for ( uint64_t i = 0; s[i]; ++i )
					if ( s[i] == ':' )
					{
						offc = i;
						break;
					}

				if ( offc != S.Ao ) // s.find(':') != std::string::npos )
				{
					int64_t const p = offc; // s.find(':');

					std::string const prefix(s,s+p); // = s.substr(0,p);
					std::string const suffix(s+p+1,s+S.Ao); // = s.substr(p+1);

					R.produced = splitSpace(prefix);
					R.dependencies = splitSpace(suffix);

					R.ignorefail = ignorefail;
					R.deepsleep = deepsleep;
					R.maxattempt = maxattempt;
					R.numthreads = numthreads;
					R.mem = mem;

					rulevalid = true;
				}
				else
				{
					libmaus2::exception::LibMausException lme;
					lme.getStream() << "[E] cannot parse line " << s << std::endl;
					lme.finish();
					throw lme;
				}
			}
		}

		if ( (++processedinputlines) % (1024*1024) == 0 )
			std::cerr << "[V] processed " << processedinputlines << " lines " << processedinputlines/static_cast<double>(numinputlines) << std::endl;
	}

	if ( rulevalid )
	{
		R.serialise(OSI);
		n += 1;
		// VR.push_back(R);
		R.clear();
	}

	std::cerr << "[V] processed " << processedinputlines << " lines " << processedinputlines/static_cast<double>(numinputlines) << std::endl;

	// return VR;

	return n;
}

#if 0
static void schdir(std::string const & d)
{
	bool done = false;

	while ( !done )
	{
		int const r = chdir(d.c_str());

		if ( r == 0 )
			done = true;
		else
		{
			int const error = errno;

			switch ( error )
			{
				case EINTR:
				case EAGAIN:
					break;
				default:
				{
					libmaus2::exception::LibMausException lme;
					lme.getStream() << "[E] chdir(" << d << "): " << strerror(error) << std::endl;
					lme.finish();
					throw lme;
				}
			}
		}
	}
}
#endif

static std::string abspath(std::string const & s)
{
	if ( ! s.size() || s[0] == '/' )
		return s;

	// current directory
	std::string const cdir = ::libmaus2::util::ArgInfo::getCurDir();

	return cdir + "/" + s;
}

static libmaus2::aio::SerialisedPeeker<Rule>::shared_ptr_type openReader(std::string const & fn)
{
	libmaus2::aio::SerialisedPeeker<Rule>::shared_ptr_type tptr(
		new libmaus2::aio::SerialisedPeeker<Rule>(fn)
	);

	return tptr;
}

int hpcschedmake(libmaus2::util::ArgParser const & arg)
{
	libmaus2::aio::SerialisedPeeker<Rule>::shared_ptr_type VLin;
	std::string const dn = arg.uniqueArgPresent("d") ? arg["d"] : getDefaultD(arg);

	std::string const fn = arg[0];

	// number of threads
	// uint64_t const numthreads = arg.uniqueArgPresent("t") ? arg.getUnsignedNumericArg<uint64_t>("t") : getDefaultNumThreads();

	std::string const rulefn = dn + ".rules";
	libmaus2::util::TempFileRemovalContainer::addTempFile(rulefn);
	uint64_t const VLn = parseFile(fn,rulefn);

	std::cerr << "[V] numer of rules is " << VLn << std::endl;

	// temp file prefix
	libmaus2::util::TempFileNameGenerator tgen(abspath(dn),4,16 /* dirmod */, 16 /* filemod */);

	std::string const producedfn = dn + ".produced";
	libmaus2::util::TempFileRemovalContainer::addTempFile(producedfn);
	std::string const depfn = dn + ".dependencies";
	libmaus2::util::TempFileRemovalContainer::addTempFile(depfn);
	struct ProducedEntry
	{
		std::string sproduced;
		uint64_t i;

		ProducedEntry() {}
		ProducedEntry(
			std::string const & rsproduced,
			uint64_t const ri
		) : sproduced(rsproduced), i(ri)
		{

		}

		std::ostream & serialise(std::ostream & ostr) const
		{
			libmaus2::util::StringSerialisation::serialiseString(ostr,sproduced);
			libmaus2::util::NumberSerialisation::serialiseNumber(ostr,i);
			return ostr;
		}

		std::istream & deserialise(std::istream & istr)
		{
			sproduced = libmaus2::util::StringSerialisation::deserialiseString(istr);
			i = libmaus2::util::NumberSerialisation::deserialiseNumber(istr);
			return istr;
		}

		bool operator<(ProducedEntry const & O) const
		{
			if ( sproduced != O.sproduced )
				return sproduced < O.sproduced;
			else
				return i < O.i;
		}
	};

	libmaus2::aio::OutputStreamInstance::unique_ptr_type ptarg(
		new libmaus2::aio::OutputStreamInstance(producedfn)
	);
	libmaus2::aio::OutputStreamInstance::unique_ptr_type pdarg(
		new libmaus2::aio::OutputStreamInstance(depfn)
	);
	VLin = openReader(rulefn);
	Rule R;
	for ( uint64_t i = 0; VLin->getNext(R); ++i )
	{
		// dependencies
		for ( uint64_t j = 0; j < R.dependencies.size(); ++j )
		{
			std::string const & sdep = R.dependencies[j];
			ProducedEntry(sdep,i).serialise(*pdarg);
		}

		for ( uint64_t j = 0; j < R.produced.size(); ++j )
		{
			std::string const & sproduced = R.produced[j];
			ProducedEntry(sproduced,i).serialise(*ptarg);
		}
	}
	ptarg->flush();
	ptarg.reset();
	pdarg->flush();
	pdarg.reset();

	libmaus2::sorting::SerialisingSortingBufferedOutputFile<ProducedEntry>::sortUnique(producedfn,128*1024);
	libmaus2::sorting::SerialisingSortingBufferedOutputFile<ProducedEntry>::sortUnique(depfn     ,128*1024);

	// check that each dependency is produced somewhere
	{
		libmaus2::aio::SerialisedPeeker<ProducedEntry> SPprod(producedfn);
		libmaus2::aio::SerialisedPeeker<ProducedEntry> SPdep(depfn);

		ProducedEntry S;
		while ( SPdep.peekNext(S) )
		{
			std::string const sdep = S.sproduced;

			while ( SPdep.peekNext(S) && S.sproduced == sdep )
				SPdep.getNext(S);

			ProducedEntry PE;
			while ( SPprod.peekNext(PE) && PE.sproduced < sdep )
				SPprod.getNext(PE);

			if ( !SPprod.peekNext(PE) || PE.sproduced != sdep )
			{
				libmaus2::exception::LibMausException lme;
				lme.getStream() << "[E] dependency " << sdep << " is not produced by any rule" << std::endl;
				lme.finish();
				throw lme;
			}

		}
	}

	struct UPair
	{
		uint64_t i;
		uint64_t j;

		UPair() {}
		UPair(uint64_t const ri, uint64_t const rj) : i(ri), j(rj) {}

		std::ostream & serialise(std::ostream & out) const
		{
			libmaus2::util::NumberSerialisation::serialiseNumber(out,i);
			libmaus2::util::NumberSerialisation::serialiseNumber(out,j);
			return out;
		}

		std::istream & deserialise(std::istream & in)
		{
			i = libmaus2::util::NumberSerialisation::deserialiseNumber(in);
			j = libmaus2::util::NumberSerialisation::deserialiseNumber(in);
			return in;
		}

		bool operator<(UPair const & U) const
		{
			if ( i != U.i )
				return i < U.i;
			else
				return j < U.j;
		}
	};

	std::string const forwfn = dn + ".forward";
	libmaus2::util::TempFileRemovalContainer::addTempFile(forwfn);
	std::string const revefn = dn + ".reverse";
	libmaus2::util::TempFileRemovalContainer::addTempFile(revefn);

	libmaus2::aio::OutputStreamInstance::unique_ptr_type pforw(
		new libmaus2::aio::OutputStreamInstance(forwfn)
	);
	libmaus2::aio::OutputStreamInstance::unique_ptr_type preve(
		new libmaus2::aio::OutputStreamInstance(revefn)
	);

	// compute dependency and reverse dependency lists
	{
		libmaus2::aio::SerialisedPeeker<ProducedEntry> SPprod(producedfn);
		libmaus2::aio::SerialisedPeeker<ProducedEntry> SPdep(depfn);

		ProducedEntry Pprod;
		ProducedEntry Pdep;
		while ( SPprod.peekNext(Pprod) && SPdep.peekNext(Pdep) )
		{
			if ( Pprod.sproduced < Pdep.sproduced )
				SPprod.getNext(Pprod);
			else if ( Pdep.sproduced < Pprod.sproduced )
				SPdep.getNext(Pdep);
			else
			{
				assert ( Pprod.sproduced == Pdep.sproduced );
				std::string const sdep = Pprod.sproduced;
				uint64_t const deppos = SPdep.tellg();

				while ( SPprod.peekNext(Pprod) && Pprod.sproduced == sdep )
				{
					SPdep.reset(deppos);
					SPprod.getNext(Pprod);

					uint64_t const producer = Pprod.i;

					while ( SPdep.peekNext(Pdep) && Pdep.sproduced == sdep )
					{
						uint64_t const dep = Pdep.i;

						UPair(dep,producer).serialise(*pforw);
						UPair(producer,dep).serialise(*preve);

						SPdep.getNext(Pdep);
					}
				}
			}
		}
	}

	pforw->flush();
	pforw.reset();
	preve->flush();
	preve.reset();

	libmaus2::sorting::SerialisingSortingBufferedOutputFile<UPair>::sortUnique(forwfn,128*1024);
	libmaus2::sorting::SerialisingSortingBufferedOutputFile<UPair>::sortUnique(revefn,128*1024);

	#if 0
	// compute target map
	struct ProducedInfo
	{
		uint64_t id;
		std::vector < uint64_t > producers;

		ProducedInfo() {}
		ProducedInfo(uint64_t const rid) : id(rid) {}
	};
	std::map<std::string,ProducedInfo> targetmap;
	{
		libmaus2::aio::SerialisedPeeker<ProducedEntry> SPprod(producedfn);
		ProducedEntry PE;

		for ( uint64_t nextid = 0; SPprod.peekNext(PE); ++nextid )
		{
			std::string const sproduced = PE.sproduced;

			ProducedInfo PI(nextid);
			while ( SPprod.peekNext(PE) && PE.sproduced == sproduced )
			{
				SPprod.getNext(PE);
				PI.producers.push_back(PE.i);
			}

			targetmap[sproduced] = PI;
		}
	}
	#endif

	// std::vector < libmaus2::util::CommandContainer > VCC(VLn);

	#if 0
	libmaus2::util::ContainerDescriptionList CDL;
	CDL.V.resize(VLn);
	#endif

	std::string const shell = "/bin/bash";
	std::string const modmagic = "hpcsched::";

	std::ostringstream cdlostrostr;
	cdlostrostr << tgen.getFileName() << ".cdl";
	std::string const cdlfn = cdlostrostr.str();
	std::string const cdlfnO = cdlfn + ".O";

	libmaus2::aio::OutputStreamInstance::unique_ptr_type cdlOSI(
		new libmaus2::aio::OutputStreamInstance(cdlfn)
	);
	libmaus2::aio::OutputStreamInstance::unique_ptr_type cdlOOSI(
		new libmaus2::aio::OutputStreamInstance(cdlfnO)
	);
	libmaus2::util::NumberSerialisation::serialiseNumber(*cdlOSI,VLn);

	VLin = openReader(rulefn);
	libmaus2::aio::SerialisedPeeker<UPair>::unique_ptr_type Sforw(new libmaus2::aio::SerialisedPeeker<UPair>(forwfn));
	libmaus2::aio::SerialisedPeeker<UPair>::unique_ptr_type Sreve(new libmaus2::aio::SerialisedPeeker<UPair>(revefn));
	for ( uint64_t id = 0; VLin->getNext(R); ++id )
	{
		std::string const in = "/dev/null";
		std::string const out = "/dev/null";
		std::string const err = "/dev/null";

		bool const modcall =
			(R.commands.size() == 1) &&
			(R.commands[0].size() >= modmagic.size()) &&
			(R.commands[0].substr(0,modmagic.size()) == modmagic);

		std::ostringstream scriptscr;
		if ( modcall )
		{
			scriptscr << R.commands[0].substr(modmagic.size());
		}
		else
		{
			// produce shell script
			scriptscr << "#! " << shell << "\n";
			scriptscr << "set -Eeuxo pipefail\n";
			for ( uint64_t i = 0; i < R.commands.size(); ++i )
				scriptscr << R.commands[i] << '\n';
			scriptscr.flush();
		}

		// construct command object
		libmaus2::util::Command C(in,out,err,shell,scriptscr.str());
		C.numattempts = 0;
		C.maxattempts = R.maxattempt;
		C.completed = false;
		C.ignorefail = R.ignorefail;
		C.deepsleep = R.deepsleep;
		C.modcall = modcall;

		libmaus2::util::CommandContainer CN;
		CN.id = id;
		CN.threads = R.numthreads;
		CN.mem = R.mem;

		std::vector<uint64_t> Vforw;
		std::vector<uint64_t> Vreve;
		UPair UP;
		while ( Sforw->peekNext(UP) && UP.i == id )
		{
			Vforw.push_back(UP.j);
			Sforw->getNext(UP);
		}
		while ( Sreve->peekNext(UP) && UP.i == id )
		{
			Vreve.push_back(UP.j);
			Sreve->getNext(UP);
		}

		CN.depid = Vforw;
		CN.rdepid = Vreve;
		CN.attempt = 0;
		CN.maxattempt = R.maxattempt;
		CN.V.push_back(C);

		std::ostringstream OSI;
		CN.serialise(OSI);

		libmaus2::util::ContainerDescription const CD(OSI.str(), false, CN.depid.size());
		CD.serialise(*cdlOSI);

		// CDL.V[id] = CD;

		uint64_t ov = CD.serialisedSize();

		libmaus2::util::NumberSerialisation::serialiseNumber(*cdlOOSI,ov);
	}
	Sforw.reset();
	Sreve.reset();

	cdlOOSI->flush();
	cdlOOSI.reset();

	// write pointer index
	libmaus2::aio::InputStreamInstance::unique_ptr_type OISI(new libmaus2::aio::InputStreamInstance(cdlfnO));
	uint64_t o = sizeof(uint64_t);
	while ( OISI->peek() != std::istream::traits_type::eof() )
	{
		libmaus2::util::NumberSerialisation::serialiseNumber(*cdlOSI,o);
		o += libmaus2::util::NumberSerialisation::deserialiseNumber(*OISI);
	}
	libmaus2::util::NumberSerialisation::serialiseNumber(*cdlOSI,o);

	cdlOSI->flush();
	cdlOSI.reset();
	OISI.reset();

	std::cout << cdlfn << std::endl;

	libmaus2::aio::FileRemoval::removeFile(rulefn);
	libmaus2::aio::FileRemoval::removeFile(producedfn);
	libmaus2::aio::FileRemoval::removeFile(depfn);
	libmaus2::aio::FileRemoval::removeFile(cdlfnO);

	std::cerr << libmaus2::util::MemUsage() << std::endl;

	return EXIT_SUCCESS;
}

int main(int argc, char * argv[])
{
	try
	{
		libmaus2::util::ArgParser const arg(argc,argv);

		if ( arg.size() < 1 )
		{
			std::cerr << "usage: " << argv[0] << " <options> Makefile" << std::endl;
			return EXIT_FAILURE;
		}
		else
		{
			int const r = hpcschedmake(arg);

			return r;
		}
	}
	catch(std::exception const & ex)
	{
		std::cerr << ex.what() << std::endl;
		return EXIT_FAILURE;
	}
}
